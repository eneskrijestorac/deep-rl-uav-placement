import dqn
from dqn_utils import *
from environment import Environment
import tensorflow as tf
import tensorflow.contrib.layers as layers
import datetime
import argparse
import os

def dqn_deep_model(observation, num_actions, scope, reuse=False):
    # Currently not used
    with tf.variable_scope(scope, reuse=reuse):
        out = observation
        with tf.variable_scope("convnet"):
            # original architecture
            # Model saved in path: models/model-2019-03-18T07:19:56.cpkt
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.nn.max_pool(out, ksize= [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.nn.max_pool(out, ksize= [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.nn.max_pool(out, ksize= [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = layers.convolution2d(out, num_outputs=32, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.nn.max_pool(out, ksize= [1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        out = layers.flatten(out)
        with tf.variable_scope("action_value"):
            out = layers.fully_connected(out, num_outputs = 512,         activation_fn=tf.nn.relu)
            out = layers.fully_connected(out, num_outputs = num_actions, activation_fn=None)

        return out


def atari_model(observation, is_training, num_actions, dropout, batch_norm, scope, reuse=False):
    # Model as described in https://storage.googleapis.com/deepmind-data/assets/papers/DeepMindNature14236Paper.pdf
    # with batchnorm and dropout
    with tf.variable_scope(scope, reuse=reuse):
        out = observation
        with tf.variable_scope("convnet"):
            # original architecture
            out = layers.convolution2d(out, num_outputs=32, kernel_size=8, stride=4, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True, reuse= False) if batch_norm else out
            #out = tf.nn.dropout(out, keep_prob = (1-dropout*tf.cast(is_training, tf.float32)))
            out = layers.convolution2d(out, num_outputs=64, kernel_size=4, stride=2, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True, reuse= False) if batch_norm else out
            #out = tf.nn.dropout(out, keep_prob = (1-dropout*tf.cast(is_training, tf.float32)))
            out = layers.convolution2d(out, num_outputs=64, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True, reuse= False) if batch_norm else out
           # out = tf.nn.dropout(out, keep_prob = (1-dropout*tf.cast(is_training, tf.float32)))
        out = layers.flatten(out)
        with tf.variable_scope("action_value"):
            out = layers.fully_connected(out, num_outputs = 512, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training ,center= True, scale=True, reuse= False) if batch_norm else out
            out = tf.nn.dropout(out, keep_prob = (1-dropout*tf.cast(is_training, tf.float32)))
            out = layers.fully_connected(out, num_outputs = num_actions, activation_fn=None)

        return out


def new_atari_model(observation, is_training, num_actions, dropout, batch_norm, scope, reuse=False):
    # Model as described in https://storage.googleapis.com/deepmind-data/assets/papers/DeepMindNature14236Paper.pdf
    # with batchnorm and dropout
    h_size =512
    with tf.variable_scope(scope, reuse=reuse):
        out = observation
        with tf.variable_scope("convnet"):
            # original architecture
            out = layers.convolution2d(out, num_outputs=32, kernel_size=8, stride=4, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=64, kernel_size=4, stride=2, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=64, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=h_size, kernel_size=8, stride=1, activation_fn=tf.nn.relu, padding = 'VALID')
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.flatten(out)
        with tf.variable_scope("advantage_value"):
            stream_A, stream_V = tf.split(out,2,1)
            AW = tf.Variable(tf.random_normal([h_size//2, num_actions]))
            VW = tf.Variable(tf.random_normal([h_size//2,1]))
            advantage = tf.matmul(stream_A, AW)
            value = tf.matmul(stream_V, VW)
            qout = value + tf.subtract(advantage, tf.reduce_mean(advantage, axis=1, keepdims=True))

        return qout


def rnn_model(observation, is_training, batch_size_ph, trace_length_ph, state_in, rnn_cell, h_size, num_actions, dropout, batch_norm, scope, reuse=False):
    # Model as described in https://storage.googleapis.com/deepmind-data/assets/papers/DeepMindNature14236Paper.pdf
    # with batchnorm and dropout
    with tf.variable_scope(scope, reuse=reuse):
        out = observation
        with tf.variable_scope("convnet"):
            # original architecture
            out = layers.convolution2d(out, num_outputs=32, kernel_size=8, stride=4, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=64, kernel_size=4, stride=2, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=64, kernel_size=3, stride=1, activation_fn=tf.nn.relu)
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
            out = layers.convolution2d(out, num_outputs=h_size, kernel_size=8, stride=1, activation_fn=tf.nn.relu, padding = 'VALID')
            out = tf.contrib.layers.batch_norm(out, decay= 0.99, is_training=is_training, center= True, scale=True) if batch_norm else out
        with tf.variable_scope("rnn"):
            out = layers.flatten(out)
            #out = tf.nn.dropout(out, keep_prob = (1-dropout*tf.cast(is_training, tf.float32)))
            out = tf.reshape(out, [batch_size_ph, trace_length_ph, h_size])
            rnn, rnn_state = tf.nn.dynamic_rnn(\
                                    inputs=out, cell=rnn_cell, dtype=tf.float32, initial_state=state_in)
            rnn = tf.reshape(rnn, shape=[-1,h_size])
        with tf.variable_scope("advantage_value"):
            stream_A, stream_V = tf.split(rnn,2,1)
            AW = tf.Variable(tf.random_normal([h_size//2, num_actions]))
            VW = tf.Variable(tf.random_normal([h_size//2,1]))
            advantage = tf.matmul(stream_A, AW)
            value = tf.matmul(stream_V, VW)
            qout = value + tf.subtract(advantage, tf.reduce_mean(advantage, axis=1, keepdims=True))

        return qout, rnn_state

models = {
            'atari_improved' : {'graph' : atari_model, 'rnn_model' : False},  
            'new_atari_model' : {'graph' : new_atari_model, 'rnn_model' : False},  
            'rnn_model' : {'graph' : rnn_model, 'rnn_model' : True}
         }

def learn(seed, session, num_timesteps, model_checkpoint, start_t):
    # Give a name to this session
    session_name = datetime.datetime.now().replace(microsecond=0).isoformat()
    

    # Define parameters
    model = 'new_atari_model'
    replay_buffer_size = 500000
    batch_size = 20
    trace_size = 4 # Only used if an RNN model is used so that batches of ordered data of trace_size can be sampled
    gamma = 0.99
    hsize = 512 # Size of the hidden layer if an RNN is used
    learning_starts = replay_buffer_size
    learning_freq = 3
    grad_norm_clipping = 10
    double_q = True
    dropout = 0.4
    batch_norm = True
    blind = False
    prioritized_replay = False
    repeat_prob = 0.4
    rnn_model = models[model]['rnn_model']
    checkpoint_interval = 1000000000
    exploration_reward = 1.2
    reward_for_reaching_stopping_sinr = 0

    # Get environment
    env = get_env(seed, repeat_prob, exploration_reward = exploration_reward, 
        reward_for_reaching_stopping_sinr = reward_for_reaching_stopping_sinr, blind = blind)

    # This is just a rough estimate
    num_iterations = float(num_timesteps) / 4.0

    lr_multiplier = 1
    lr_schedule = PiecewiseSchedule([
                                         (0,                   1e-4 * lr_multiplier),
                                         (1.5e6, 5e-5 * lr_multiplier),
                                         (2e6,  5e-5 * lr_multiplier),
                                    ],
                                    outside_value=1e-5 * lr_multiplier)
    optimizer = dqn.OptimizerSpec(
        constructor=tf.train.AdamOptimizer,
        kwargs=dict(epsilon=1e-4),
        lr_schedule=lr_schedule
    )

    def stopping_criterion(env, t):
        # notice that here t is the number of steps of the wrapped env,
        # which is different from the number of steps in the underlying env
        return env.get_total_steps() >= num_timesteps

    exploration_schedule = PiecewiseSchedule(
        [
            (0, 1.0),
            (3e6, 0.1),
            (num_iterations / 2, 0.01),
        ], outside_value=0.01
    )

    alg = dqn.QLearner(
        checkpoint_interval = checkpoint_interval,
        env=env,
        q_func=models[model]['graph'],
        optimizer_spec=optimizer,
        session=session,
        session_name = session_name,
        exploration=exploration_schedule,
        stopping_criterion=stopping_criterion,
        replay_buffer_size = replay_buffer_size,
        batch_size=batch_size,
        trace_size = trace_size, 
        gamma=gamma,
        learning_starts = learning_starts,
        learning_freq = learning_freq,
        frame_history_len=1,
        target_update_freq=10000,
        grad_norm_clipping=grad_norm_clipping,
        double_q = double_q,
        dropout = dropout, 
        rnn_model = rnn_model,
        batch_norm = batch_norm,
        prioritized_replay = prioritized_replay,
        model_checkpoint = model_checkpoint,
        start_t = start_t,
        hsize = hsize
    )

    store_configuration(
        session_name,
        model = model,
        blind = blind,
        lr_multiplier = lr_multiplier,
        num_timesteps = num_timesteps, 
        replay_buffer_size = replay_buffer_size,
        batch_size = batch_size,
        gamma = gamma,
        exploration_reward = exploration_reward,
        reward_for_reaching_stopping_sinr = reward_for_reaching_stopping_sinr,
        learning_starts = learning_starts,
        learning_freq = learning_freq,
        grad_norm_clipping = grad_norm_clipping,
        double_q = double_q,
        dropout = dropout,
        batch_norm = batch_norm,
        rnn_model = rnn_model,
        trace_size = trace_size,
        prioritized_replay = prioritized_replay,
        repeat_prob = repeat_prob,
        seed = seed,
        start_t = start_t,
        model_checkpoint = model_checkpoint,
        hsize = hsize,
        gpus = get_available_gpus()
        )
    while not alg.stopping_criterion_met():
        alg.step_env()
        alg.update_model()
        alg.log_progress()

    env.close()

def store_configuration(session_name, **kwargs):

    config_file = "logs/config-" + session_name +'.log'
    f = open(config_file, 'w')
    f.write('parameter,value\n')
    for key in kwargs.keys():
        f.write('%s,%s\n' % (key, str(kwargs[key])))
    f.close()

def get_available_gpus():
    from tensorflow.python.client import device_lib
    local_device_protos = device_lib.list_local_devices()
    return [x.physical_device_desc for x in local_device_protos if x.device_type == 'GPU']

def set_global_seeds(i):
    try:
        import tensorflow as tf
    except ImportError:
        pass
    else:
        tf.set_random_seed(i)
    np.random.seed(i)
    random.seed(i)

def get_session():
    tf.reset_default_graph()
    tf_config = tf.ConfigProto(
        inter_op_parallelism_threads=1,
        intra_op_parallelism_threads=1,)
    session = tf.Session(config=tf_config)
    print("AVAILABLE GPUS: ", get_available_gpus())
    return session

def get_env(seed, repeat_prob, exploration_reward, reward_for_reaching_stopping_sinr, blind):
    env = Environment(n_uavs = 1, n_users = 1, repeat_probability = repeat_prob, seed = seed, exploration_reward = exploration_reward, 
        reward_for_reaching_stopping_sinr = reward_for_reaching_stopping_sinr, train = True, blind = blind)

    set_global_seeds(seed)

    return env

def get_test_env(seed, max_episode_length, n_uavs, n_users, assignment, blind):
    env = Environment(seed = seed, max_episode_length = max_episode_length, n_uavs = n_uavs, n_users = n_users, assignment = assignment, train = False, blind = blind)

    set_global_seeds(seed)

    return env



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Train and test the DQN.')
    parser.add_argument('--model-checkpoint', dest='model_checkpoint', 
                action='store', help = "If included, the model will be initialized from this file.", default = "")
    parser.add_argument('--start-t', dest='start_t', 
                action='store', help = "If included, training will start from this point.", default = 0, type = int)
    args = parser.parse_args()
    
    # Uncoment if you want to run on CPU
    #os.environ['CUDA_VISIBLE_DEVICES'] = ''

    # First check if we are restoring a model
    model_checkpoint = args.model_checkpoint
    start_t = args.start_t

    seed = 4276
    #seed = random.randint(0, 9999)
    print('random seed = %d' % seed)
    session = get_session()
    learn(seed, session, num_timesteps=1e9, start_t = start_t, model_checkpoint = model_checkpoint)


