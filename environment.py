import numpy as np
import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join
import random 
import scipy.optimize
import itertools
import time
plt.rcParams.update({'font.size': 24})

SINR_GRIDS_DIRECTORY = 'stored_grids/sinr_grids'
CITY_GRIDS_DIRECTORY = 'stored_grids/city_grids'
OBSERVATION_WIDTH = 60 # How many fields around it can the UAV observe
COLLISION_REWARD = 0 # Punish the agent for colliding with buildings
STOPPING_SINR = 5.00 # Stop the motion once this SINR has been reached
UAV_ALTITUDE = 15.00
USER_ALTITUDE = 2
REWARD_FOR_REACHING_STOPPING_SINR = 250
NOISE_LEVEL = -104.01029 # As defined in the simulator
SINR_OF_AN_UNCOVERED_FIELD = 50
SINR_OF_AN_INACCESSIBLE_FIELD = -145.9  # Defined in the ray tracing software
MAX_EPISODE_LENGTH = 800
EXPLORATION_REWARD = 1
HEIGHT_OF_OFFMAP_TERRAIN = 50
GRID_LEN = 4

def get_all_permuation_matrices(N):
	A = np.eye(N)
	all_permutations = []
	for m in itertools.permutations(A):
	    all_permutations.append(np.asarray(m))
	return all_permutations

class EnviornmentState:
	def __init__(self, flying_altitude, building_grid, sinr_grids, exploration_reward, reward_for_reaching_stopping_sinr, max_episode_length, n_uavs, n_users, assignment):
		self.xsize = sinr_grids[0].shape[1] # All the sinr grids will have the same shape
		self.ysize = sinr_grids[0].shape[0]
		self.n_uavs = n_uavs
		self.n_users = n_users
		self.assignment = assignment
		self.flying_altitude = flying_altitude
		self.exploration_reward = exploration_reward
		self.max_episode_length = max_episode_length
		self.user_association = np.arange(n_users) # The index of this list corresponds to the index of the UAV
		self.reward_for_reaching_stopping_sinr = reward_for_reaching_stopping_sinr
		self.coordinates = np.zeros((n_uavs, 2), dtype = np.int) # Coordinates for each UAV in the environment

		# Randomize the starting locations of the UAVs
		for i in range(n_uavs):
			while(1):
				# Generate a state that is located in a valid location
				coordinates = [random.randint(0, self.xsize-1), random.randint(0, self.ysize-1)]
				if building_grid[coordinates[1], coordinates[0]] <=  self.flying_altitude: # Means there is no building here
					if any(sinr_grids[:, coordinates[1], coordinates[0]] > (SINR_OF_AN_INACCESSIBLE_FIELD)): # Means that the UAV has some reception
						if all(sinr_grids[:, coordinates[1], coordinates[0]] < STOPPING_SINR): # Means that the UAV will have to move towards stopping SINR				
							break
			self.coordinates[i, :] = coordinates

		self.episode_reward = 0
		self.sinr_improvements = np.zeros(n_uavs)
		self.reached_stopping = np.zeros(n_uavs, dtype = np.bool)
		self.n_steps = np.zeros(n_uavs, dtype = np.int) # Each UAV counts its own number of updates
		self.completed = np.zeros(n_uavs, dtype = np.int)
		self.completed_users = []
		self.last_actions = np.zeros(n_uavs)

		self.uncovered_grids = np.zeros((n_uavs, n_users, self.ysize, self.xsize),) + SINR_OF_AN_UNCOVERED_FIELD
		self.sinr_grids = sinr_grids
		self.building_grid = building_grid

		self.current_sinrs = self.sinr_grids[self.user_association, self.coordinates[:, 1], self.coordinates[:, 0]]
		self.best_sinrs = self.current_sinrs
		self.start_sinrs = np.copy(self.current_sinrs)
		for uav_idx in range(n_uavs):
			self.uncovered_grids[uav_idx,:, self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]] = self.sinr_grids[:, self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]] # The agent can measure the SINR at the current location, 
																					

	def make_random_assignment(self):
		unconverged_uavs = np.arange(self.n_uavs)[self.reached_stopping == 0]
		unconverged_users = self.user_association[unconverged_uavs]
		np.random.shuffle(unconverged_users)	
		self.user_association[unconverged_uavs] =  unconverged_users

	def make_sinr_based_assignment(self, scale, offset):
		#self.completed = np.zeros(self.n_uavs, dtype = np.int)
		#self.reached_stopping = np.zeros(self.n_uavs, dtype = np.bool)
		unconverged_uavs = np.arange(self.n_uavs)[self.reached_stopping == 0]
		unconverged_users = self.user_association[unconverged_uavs]

		S = self.sinr_grids[:, self.coordinates[unconverged_uavs, 1], self.coordinates[unconverged_uavs, 0]][unconverged_users, :].T # SINR matrix


		offset = offset[unconverged_uavs, :][:, unconverged_uavs]		

		S = scale * S + offset

		row_ind, col_ind = scipy.optimize.linear_sum_assignment(-S)
		self.user_association[unconverged_uavs[row_ind]] = unconverged_users[col_ind]

	def get_distance_matrix(self):

		ground_user_locations = []
		for i in range(self.n_users):
			indmax = np.argmax(self.sinr_grids[i,:])
			coord = np.unravel_index(indmax, self.sinr_grids[i,:].shape)
			coord = [coord[1], coord[0]]
			ground_user_locations.append(coord)
		ground_user_locations = np.asarray(ground_user_locations)

		coords_matrix = np.kron(np.ones((1, self.n_uavs, 1)), self.coordinates.reshape((self.n_uavs, 1, 2))) - \
								np.kron(np.ones((self.n_users, 1 , 1)), ground_user_locations.reshape((1, self.n_users, 2)))

		distance_matrix = np.linalg.norm(coords_matrix, axis = 2)
		return distance_matrix

	def make_distance_based_assignment(self):
		unconverged_uavs = np.arange(self.n_uavs)[self.reached_stopping == 0]
		unconverged_users = self.user_association[unconverged_uavs]
		distance_matrix = self.get_distance_matrix()
		distance_matrix = distance_matrix[unconverged_uavs, :][:, unconverged_users]
		row_ind, col_ind = scipy.optimize.linear_sum_assignment(distance_matrix)
		self.user_association[unconverged_uavs[row_ind]] = unconverged_users[col_ind]

	def update(self, action_space, action, uav_idx):
		if self.completed[uav_idx]:
			raise KeyError('This UAV has completed its episode.')

		dx, dy = action_space.get_displacement(action)
		viable_actions = action_space.get_viable_actions(self, uav_idx)
		self.last_actions[uav_idx] = action

		# Update the coordinates

		self.coordinates[uav_idx, 0] = self.coordinates[uav_idx, 0] + dx
		self.coordinates[uav_idx,1] = self.coordinates[uav_idx, 1] + dy


		if self.n_steps[uav_idx] >= self.max_episode_length:
			# Means that max episode length has been exceeded
			done = True
			self.completed[uav_idx] = 1
			reward = 0
			self.sinr_improvements[uav_idx] = self.current_sinrs[uav_idx] - self.start_sinrs[uav_idx]

		else:
			

			# Update the current sinr
			old_sinr = self.current_sinrs[uav_idx]
			self.current_sinrs[uav_idx] = self.sinr_grids[self.user_association[uav_idx], self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]]
			# Calculate the reward
			reward = self.current_sinrs[uav_idx]- old_sinr

			# Update the best snr
			self.best_sinrs[uav_idx] = max(self.best_sinrs[uav_idx], self.current_sinrs[uav_idx])

			if self.current_sinrs[uav_idx] >= STOPPING_SINR:
				reward += self.reward_for_reaching_stopping_sinr
				self.sinr_improvements[uav_idx] = self.current_sinrs[uav_idx] - self.start_sinrs[uav_idx]
				self.reached_stopping[uav_idx] = True
				self.completed[uav_idx] = 1
				self.completed_users.append(self.user_association[uav_idx])
				done = True

			else:
				# Add an exploration reward
				if all(self.uncovered_grids[uav_idx, :, self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]] == SINR_OF_AN_UNCOVERED_FIELD):
					reward += self.exploration_reward

				# Update the explored grid
				self.uncovered_grids[uav_idx,:, self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]] = self.sinr_grids[:, self.coordinates[uav_idx, 1], self.coordinates[uav_idx, 0]]
				done = False



		self.episode_reward += reward 
		self.n_steps[uav_idx] += 1
		return reward, done


	


class ObservationSpace:
	def __init__(self, shape):
		self.shape = shape

class ActionSpace:
	def __init__(self, n_uavs, flying_altitude, repeat_probability):
		self.n_uavs = n_uavs
		self.n = 4
		self.step_size = 1
		self.repeat_probability = repeat_probability
		self.flying_altitude = flying_altitude

	def get_random_action(self, state, uav_idx):
		if random.random() < self.repeat_probability:
			action = state.last_actions[uav_idx]
			dx, dy = self.get_displacement(action)
			if (state.coordinates[uav_idx, 0] + dx) < 0 or (state.coordinates[uav_idx, 0] + dx) > (state.xsize-1):
				pass
			elif (state.coordinates[uav_idx, 1] + dy) < 0 or (state.coordinates[uav_idx, 1] + dy) > (state.ysize-1):
				pass
			elif state.building_grid[state.coordinates[uav_idx, 1] + dy, state.coordinates[uav_idx, 0] + dx] > self.flying_altitude :
				pass
			else:
				return action


		while(1):
			action = random.randint(0, self.n -1)
			dx, dy = self.get_displacement(action)
			if (state.coordinates[uav_idx, 0] + dx) < 0 or (state.coordinates[uav_idx, 0] + dx) > (state.xsize-1):
				pass
			elif (state.coordinates[uav_idx, 1] + dy) < 0 or (state.coordinates[uav_idx, 1] + dy) > (state.ysize-1):
				pass
			elif state.building_grid[state.coordinates[uav_idx, 1] + dy, state.coordinates[uav_idx, 0] + dx] > self.flying_altitude :
				pass
			else:
				return action

	def get_viable_actions(self, state, uav_idx):
		# Returns a one-hot vector denoting if an action is viable or not
		viable_actions = np.zeros(self.n)
		for action in range(self.n):	
			dx, dy = self.get_displacement(action)
			if (state.coordinates[uav_idx, 0] + dx) < 0 or (state.coordinates[uav_idx, 0] + dx) > (state.xsize-1):
				pass
			elif (state.coordinates[uav_idx, 1] + dy) < 0 or (state.coordinates[uav_idx, 1] + dy) > (state.ysize-1):
				pass
			elif state.building_grid[state.coordinates[uav_idx, 1] + dy, state.coordinates[uav_idx, 0] + dx] > self.flying_altitude :
				pass
			else:
				viable_actions[action] = 1
		return viable_actions


	def get_displacement(self, action):
		if action == 0:
			# UP
			dx = 0; dy = -self.step_size
		elif action == 1:
			# DOWN
			dx = 0; dy = self.step_size
		elif action == 2:
			# LEFT
			dx = -self.step_size; dy = 0
		elif action == 3:
			# RIGHT
			dx = self.step_size; dy = 0
		else:
			raise ValueError('Ilegal action.')	

		return dx, dy

class Environment:
	'''
	Simulates the environment of a UAV moving in an urban landscape.
	As of now, the UAV is only capable of moving in 2D. The space is partitioned into 
	a grid. 
	The possible actions are one step up, down, left or right. The step size is the same
	as one grid block size. 
	'''
	def __init__(self, n_uavs = 1, n_users = 1, repeat_probability = 0.0, seed = 3123213, exploration_reward = EXPLORATION_REWARD, blind = False,
			reward_for_reaching_stopping_sinr = REWARD_FOR_REACHING_STOPPING_SINR, max_episode_length = MAX_EPISODE_LENGTH, assignment = '', train = True):
		"""
		repeat_probability: defines the probability of repeating the previous step when doing random_exploration
		"""
		random.seed(seed)
		self.n_users = n_users
		self.n_uavs = n_uavs
		self.train = train
		self.assignment = assignment # Type of assignment done
		self.observation_width = OBSERVATION_WIDTH + 1 # One central pixel for the agent, so it must be odd
		self.exploration_reward = exploration_reward
		self.reward_for_reaching_stopping_sinr = reward_for_reaching_stopping_sinr
		self.max_episode_length = max_episode_length
		self.blind = blind
		if blind:
			self.channels = 1
		else:
			self.channels = 2 
		self.flying_altitude =  UAV_ALTITUDE
		self.observation_space = ObservationSpace((self.observation_width, self.observation_width, self.channels))
		self.action_space = ActionSpace(n_uavs, self.flying_altitude, repeat_probability) # Only four possible discrete actions
		self.episode_rewards = []
		self.episode_lengths = []
		self.sinr_improvements = []
		self.success_history = []
		self.total_steps = np.zeros(n_uavs)
		self.sinr_grids, self.building_grid = self.load_grids(n_users) # Loads a random set of grids
		self.state = EnviornmentState(self.flying_altitude, self.building_grid, self.sinr_grids,
					self.exploration_reward, self.reward_for_reaching_stopping_sinr, self.max_episode_length, n_uavs, n_users, assignment)
		self.state_history = {}

		assert self.building_grid.shape == self.sinr_grids[0].shape
		assert (self.observation_width-1) % 2 == 0

	def reset(self):		
		self.episode_rewards.append(self.state.episode_reward)
		self.sinr_improvements.append(self.state.sinr_improvements)
		self.episode_lengths.append(self.state.n_steps)
		self.success_history.append(self.state.reached_stopping)
		self.sinr_grids, self.building_grid = self.load_grids(self.n_users) # Loads a random set of grids
		self.state = EnviornmentState(self.flying_altitude, self.building_grid, self.sinr_grids,
					self.exploration_reward, self.reward_for_reaching_stopping_sinr, self.max_episode_length, self.n_uavs, self.n_users, self.assignment)
		self.state_history = {}
		return self.get_observation()

	def load_grids(self, n_users):
		if self.train == True:
			d_type = 'train'
		else:
			d_type = 'test'

		random_rotation = random.randint(0, 3)
		# Get a city grid file
		all_city_grids = [f for f in listdir(CITY_GRIDS_DIRECTORY) if (isfile(join(CITY_GRIDS_DIRECTORY, f)) and (d_type in f) )] # The grids use to be rotated which is why																								 # we had different city files
		all_sinr_grids = [join(SINR_GRIDS_DIRECTORY, f) for f in listdir(SINR_GRIDS_DIRECTORY) if (isfile(join(SINR_GRIDS_DIRECTORY, f)) and (d_type in f) )]
		city_grid_file = join(CITY_GRIDS_DIRECTORY, random.choice(all_city_grids))
		city_grid = np.rot90(np.loadtxt(city_grid_file, dtype=float), k = random_rotation)
		sinr_grids = np.zeros((n_users, city_grid.shape[0], city_grid.shape[1]))
		for i in range(n_users):
			sinr_grid_file = random.choice(all_sinr_grids)
			sinr_grid = np.loadtxt(sinr_grid_file, dtype=float)	

			sinr_grids[i, :, :] = np.rot90(sinr_grid, k = random_rotation)
		
		return sinr_grids, city_grid

	def get_random_action(self, uav_idx = 0):
		return self.action_space.get_random_action(self.state, uav_idx)

	def get_viable_actions(self, uav_idx = 0):
		return self.action_space.get_viable_actions(self.state, uav_idx)

	def get_episode_rewards(self):
		return np.asarray(self.episode_rewards[1:])

	def get_sinr_improvements(self):
		return np.asarray(self.sinr_improvements[1:])

	def get_episode_lengths(self):
		return np.asarray(self.episode_lengths[1:])

	def get_success_history(self):
		return np.asarray(self.success_history[1:])

	def get_episode_reward(self):
		return self.state.episode_reward

	def get_episode_sinr_improvement(self, uav_idx):
		return self.state.sinr_improvements[uav_idx]

	def get_episode_length(self, uav_idx):
		return self.state.n_steps[uav_idx]

	def get_episode_success(self, uav_idx):
		return self.state.reached_stopping[uav_idx]

	def make_sinr_based_assignment(self, scale, offset):
		self.state.make_sinr_based_assignment(scale, offset)

	def make_random_assignment(self):
		self.state.make_random_assignment()

	def make_distance_based_assignment(self):
		self.state.make_distance_based_assignment()

	def get_observations_for_all_users(self):
		observations = []
		for uav_idx in range(self.n_uavs):
			for user_idx in range(self.n_users):
				observations.append(self.get_observation(uav_idx, user_idx))
		return observations

	def get_observations(self):
		'''
		Returns a list of observations for each UAV
		'''	
		observations = []
		for uav_idx in range(self.n_uavs):
			observations.append(self.get_observation(uav_idx))
		return observations

	def get_observation(self, uav_idx = 0, user_idx = -1):
		if user_idx == -1:
			user_idx = self.state.user_association[uav_idx]

		if self.blind:
			sinr_observation = self.get_sinr_observation(uav_idx, user_idx)
			# Scale the observations
			sinr_observation = sinr_observation / SINR_OF_AN_INACCESSIBLE_FIELD
			return sinr_observation
		else:
			sinr_observation = self.get_sinr_observation(uav_idx, user_idx)
			city_observation = self.get_city_observation(uav_idx)

			accessible = city_observation <= 0 # Since the observation is aleready offset by UAV height
			sinr_observation = sinr_observation * accessible + (1-accessible) * SINR_OF_AN_INACCESSIBLE_FIELD # If we can't access the field, we know there is no signal there
			
			# Scale the observations
			sinr_observation = sinr_observation / SINR_OF_AN_INACCESSIBLE_FIELD
			city_observation = city_observation / max(float(np.max(self.building_grid)), float(self.flying_altitude))
			
			return np.stack([city_observation, sinr_observation], axis = 2)

	def get_sinr_observation(self, uav_idx, user_idx):
		xcoord = self.state.coordinates[uav_idx, 0]
		ycoord = self.state.coordinates[uav_idx, 1]
		ow = self.observation_width

		xsize = self.building_grid.shape[1]
		ysize = self.building_grid.shape[0]

		# Slice a piece of the building grid and pad with ones if necessary to get a square
		axis_1_padding = (-min(0, xcoord - int(ow/2)), -min(0, xsize - 1 - xcoord - int(ow/2)))
		axis_0_padding = (-min(0, ycoord - int(ow/2)), -min(0, ysize - 1 - ycoord - int(ow/2)))		

		# Slice the current map of the sinr
		sinr_observation = self.state.uncovered_grids[uav_idx, user_idx, max(0, ycoord- int(ow/2)) : 1 + min(ysize-1, ycoord + int(ow/2)), max(0, xcoord-int(ow/2)) : 1 + min(xsize-1, xcoord + int(ow/2))]

		# Padd with -250 db (this is the lowest value from the simulator) to ensure constant width and height
		sinr_observation = np.pad(sinr_observation, (axis_0_padding, axis_1_padding), 'constant', constant_values = SINR_OF_AN_INACCESSIBLE_FIELD)

		return sinr_observation


	def get_city_observation(self, uav_idx):
		xcoord = self.state.coordinates[uav_idx, 0]
		ycoord = self.state.coordinates[uav_idx, 1]
		ow = self.observation_width

		xsize = self.building_grid.shape[1]
		ysize = self.building_grid.shape[0]

		# Slice a piece of the building grid and pad with ones if necessary to get a square
		axis_1_padding = (-min(0, xcoord - int(ow/2)), -min(0, xsize - 1 - xcoord - int(ow/2)))
		axis_0_padding = (-min(0, ycoord - int(ow/2)), -min(0, ysize - 1 - ycoord - int(ow/2)))
		
		# Slice the city grid
		city_observation = self.building_grid[max(0, ycoord- int(ow/2)) : 1 + min(ysize-1, ycoord + int(ow/2)), max(0, xcoord-int(ow/2)) : 1 + min(xsize-1, xcoord + int(ow/2))]

		# Padd with 1s to ensure constant width and height
		city_observation = np.pad(city_observation, (axis_0_padding, axis_1_padding), 'constant', constant_values = HEIGHT_OF_OFFMAP_TERRAIN)
		city_observation = city_observation - self.flying_altitude # Subtract the UAV altitude


		return city_observation

	def step(self, action, uav_idx = 0):
		self.total_steps[uav_idx] += 1

		if not(uav_idx in self.state_history.keys()):
			self.state_history[uav_idx] = []
			self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))
		else:
			self.state_history[uav_idx].append(np.copy(self.state.coordinates[uav_idx, :]))

	
		reward, done = self.state.update(self.action_space, action, uav_idx)
		next_obs = self.get_observation(uav_idx)
		info = ''

		return next_obs, reward, done, info


	def render(self, uav_idx):
		self.fig1, (self.ax1, self.ax2) = plt.subplots(2, 1)


		indmax = np.argmax(self.sinr_grids[self.state.user_association[uav_idx]])
		coord = np.unravel_index(indmax, self.sinr_grids[self.state.user_association[uav_idx]].shape)
		coord = [coord[1], coord[0]]


		im1 = self.ax1.imshow(self.building_grid, cmap='Pastel2')
		self.ax1.scatter([s[0] for s in self.state_history[uav_idx][1:]], [s[1] for s in self.state_history[uav_idx][1:]], c='r', s=5)
		self.ax2.scatter([self.state_history[uav_idx][0][0]], [self.state_history[uav_idx][0][1]], c='g', s=10)
		self.ax2.scatter([coord[0]], [coord[1]], c='black', s=10)
		self.ax1.set(xlabel='x (m)', ylabel='y (m)',
          title='Trajectory')
		self.ax1.set_xticks(np.arange(self.building_grid.shape[1], step = 50))
		self.ax1.set_xticklabels(np.arange(self.building_grid.shape[1], step = 50)*GRID_LEN)
		self.ax1.set_yticklabels(np.arange(self.building_grid.shape[0], step = 20)*GRID_LEN)
		self.ax1.set_yticks(np.arange(self.building_grid.shape[0], step = 20))
		cbar1 = self.ax1.figure.colorbar(im1, ax=self.ax1, cmap='Pastel2', fraction=0.046, pad=0.04)
		cbar1.ax.set_ylabel('Height (m)', rotation=-90, va="bottom")
		
		im2 = self.ax2.imshow(self.sinr_grids[self.state.user_association[uav_idx]], cmap='BuPu')
		self.ax2.scatter([s[0] for s in self.state_history[uav_idx][1:]], [s[1] for s in self.state_history[uav_idx][1:]], c='r', s=5)
		self.ax2.scatter([self.state_history[uav_idx][0][0]], [self.state_history[uav_idx][0][1]], marker = '^', c='g', s=40)
		self.ax2.scatter([coord[0]], [coord[1]], marker = '^', c='blue', s=40)
		self.ax2.set(xlabel='x (m)', ylabel='y (m)')
		self.ax2.set_xticks(np.arange(self.building_grid.shape[1], step = 50))
		self.ax2.set_xticklabels(np.arange(self.building_grid.shape[1], step = 50)*GRID_LEN)
		self.ax2.set_yticklabels(np.arange(self.building_grid.shape[0], step = 20)*GRID_LEN)
		self.ax2.set_yticks(np.arange(self.building_grid.shape[0], step = 20))
		cbar2 = self.ax2.figure.colorbar(im2, ax=self.ax2, cmap='BuPu', fraction=0.046, pad=0.04)
		cbar2.ax.set_ylabel('SINR (dB)', rotation=-90, va="bottom")
		plt.show()
	
	def render_current_obs(self, uav_idx):	
		user_idx = self.state.user_association[uav_idx]
		self.obs_fig, (self.obs_ax1, self.obs_ax2) = plt.subplots(1, 2)
		plt.tight_layout()
		
		sinr_observation = self.get_sinr_observation(uav_idx, user_idx)
		city_observation = self.get_city_observation(uav_idx)
		inaccessible = city_observation > 0
		
		sinr_observation[inaccessible] =  SINR_OF_AN_INACCESSIBLE_FIELD # If we can't access the field, we know there is no signal there
		im1 = self.obs_ax1.imshow(city_observation, cmap='Blues')
		cbar1 = self.obs_ax1.figure.colorbar(im1, ax=self.obs_ax1, cmap='Blues', fraction=0.046, pad=0.04)
		cbar1.ax.set_ylabel('Height', rotation=-90, va="bottom")
		im2 = self.obs_ax2.imshow(sinr_observation, cmap='BuPu')
		cbar2 = self.obs_ax2.figure.colorbar(im2, ax=self.obs_ax2, cmap='BuPu', fraction=0.046, pad=0.04)
		cbar2.ax.set_ylabel('SINR (dB)', rotation=-90, va="bottom")
		self.obs_ax1.set(xlabel='x', ylabel='y',
          title='Area map')
		self.obs_ax2.set(xlabel='x', ylabel='y',
          title='Previous SINR values')
		plt.show()

	def get_maximum_expected_sinr_increase():
		all_sinr_grids =[join(SINR_GRIDS_DIRECTORY, f) for f in listdir(SINR_GRIDS_DIRECTORY) if (isfile(join(SINR_GRIDS_DIRECTORY, f)) and ('train' in f) )]
		train_ranges = []
		for sinr_grid_file in  all_sinr_grids:
			# Find mean SINR range
			sinr_grid = np.loadtxt(sinr_grid_file, dtype=float)
			mean_sinr_range = np.mean(STOPPING_SINR - sinr_grid[(sinr_grid < STOPPING_SINR) * (sinr_grid > SINR_OF_AN_INACCESSIBLE_FIELD)])
			train_ranges.append(mean_sinr_range)

		all_sinr_grids =[join(SINR_GRIDS_DIRECTORY, f) for f in listdir(SINR_GRIDS_DIRECTORY) if (isfile(join(SINR_GRIDS_DIRECTORY, f)) and ('test' in f) )]
		test_ranges = []
		for sinr_grid_file in  all_sinr_grids:
			# Find mean SINR range
			sinr_grid = np.loadtxt(sinr_grid_file, dtype=float)
			mean_sinr_range = np.mean(STOPPING_SINR - sinr_grid[(sinr_grid < STOPPING_SINR) * (sinr_grid > SINR_OF_AN_INACCESSIBLE_FIELD)])
			test_ranges.append(mean_sinr_range)
		print(np.mean(train_ranges), np.mean(test_ranges))
		outfile = 'stats/'+'expected_sinr_increase'
		np.savez(outfile, train = np.mean(train_ranges), test = np.mean(test_ranges))
			


	def close(self):
		plt.close('all')
		print('Environment closed.')

	def get_total_steps(self):
		return self.total_steps

	def get_current_sinr(self):
		return self.state.current_sinr

if __name__ == "__main__":
	all_permutations = get_all_permuation_matrices(3)