import random 
import sys
import numpy as np
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
from .utils import load_grids, plot_nodes
np.set_printoptions(threshold=sys.maxsize)

seed = 4273
SINR_GRIDS_DIRECTORY = 'stored_grids/sinr_grids'
CITY_GRIDS_DIRECTORY = 'stored_grids/city_grids'
STOPPING_SINR = 5.00
NOISE_LEVEL = 145.00
GRID_LEN = 4
#random.seed(seed)





class TerminalNode:
	def __init__(self, x, y):
		self.coordinates = np.asarray([x, y])
		self.distances = np.asarray([])

class Vertex:
	def __init__(self, x, y, starting_sinr):
		self.coordinates = np.asarray([x, y])
		self.distances = np.asarray([])
		self.starting_sinr = starting_sinr

if __name__ == "__main__":
	# Get all terminal nodes
	sinr_grid, city_grid = load_grids()
	terminal_nodes_grid = sinr_grid >= STOPPING_SINR
	terminal_nodes = []
	x_size = sinr_grid.shape[1]
	y_size = sinr_grid.shape[0]

	# Find mean SINR range
	mean_sinr_range = np.mean(STOPPING_SINR - sinr_grid[(sinr_grid < STOPPING_SINR) * (sinr_grid > -NOISE_LEVEL)])
	print('Mean SINR range: %f' % (mean_sinr_range))


	for xidx in range(x_size):
		for yidx in range(y_size):
			if terminal_nodes_grid[yidx, xidx]:
				# We only want the outer terminal nodes
				if (xidx - 1) < 0 or ((xidx - 1) >= 0 and terminal_nodes_grid[yidx, xidx - 1]): 
					if (xidx + 1) >= x_size or ((xidx + 1) < x_size and terminal_nodes_grid[yidx, xidx + 1]):
						if (yidx - 1) < 0 or ((yidx - 1) >= 0 and terminal_nodes_grid[yidx - 1, xidx]): 
							if (yidx + 1) >= x_size or ((yidx + 1) < y_size and terminal_nodes_grid[yidx + 1, xidx]):	
								continue			 
				terminal_nodes.append(TerminalNode(xidx, yidx))

	#plot_nodes(city_grid, sinr_grid, terminal_nodes)

	# Get all starting nodes
	starting_nodes = []
	starting_nodes_grid = (sinr_grid < STOPPING_SINR) * (sinr_grid > -NOISE_LEVEL)
	for xidx in range(x_size):
		for yidx in range(y_size):
			if starting_nodes_grid[yidx, xidx]:
				starting_nodes.append(Vertex(xidx, yidx, sinr_grid[yidx, xidx]))


	#plot_nodes(city_grid, sinr_grid, starting_nodes)

	all_nodes = starting_nodes + terminal_nodes

	# Pick a vertex t as a starting point
	t =  random.choice(all_nodes[0:len(starting_nodes)])


	# Run Bellman-Ford algorithm
	# See https://www.cs.cmu.edu/~avrim/451f12/lectures/lect1002.pdf

	t.distances = np.zeros((len(all_nodes), len(all_nodes)-1)) + np.Inf

	# First calculate one-step distances
	one_step_distances = np.zeros((len(all_nodes), len(all_nodes)))
	all_coordinates = np.asarray([v.coordinates for v in all_nodes])
	for i in range(len(all_nodes)):
		separation = (all_coordinates - all_coordinates[i,:])
		one_step_distances[i, :] = np.sqrt(np.square(separation[:, 0]) + np.square(separation[:, 1]))
	one_step_distances[one_step_distances >1] += np.Inf  

	max_steps = 300
	done = False
	steps_needed = 0
	for i in range(max_steps):
		print('Progress %d / %d' % (i, max_steps))
		for j in range(len(all_nodes)):
			v = all_nodes[j]
			if v != t:
				if i != 0: 
					# Find i-1 step away nodes that is the closest
					distances = t.distances[:, i - 1] + one_step_distances[j, :]
					# Remove infinity distances
					distances = distances[ distances < np.Inf ]
					if len(distances):
						min_distance = np.min(distances)
					else:
						min_distance = np.Inf
					t.distances[j, i] = min_distance
			else:
				t.distances[j, i] = 0
		if np.sum(t.distances[len(starting_nodes) : , i] < np.Inf):
			print('Found the shortest path to a point with high enough SINR.')
			print('The SINR increase is %f.' % (STOPPING_SINR - t.starting_sinr))
			steps_needed = i
			break

	shortest_path = []
	for i in range(steps_needed, -1, -1):
		if i == steps_needed:
			next_node = np.argmin(t.distances[len(starting_nodes) : , i] ) + len(starting_nodes)
			t.distances[len(starting_nodes) : , i]
		else:
			next_node = np.argmax((t.distances[: , i] + one_step_distances[next_node, :]) == t.distances[next_node , i + 1] )
			shortest_path.append(next_node)


	nodes_on_the_shortest_path = [all_nodes[p] for p in shortest_path]

	plot_nodes(city_grid, sinr_grid, nodes_on_the_shortest_path)