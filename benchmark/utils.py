import random 
import sys
import numpy as np
from environment import GRID_LEN
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
from environment import CITY_GRIDS_DIRECTORY, SINR_GRIDS_DIRECTORY, NOISE_LEVEL
np.set_printoptions(threshold=sys.maxsize)

def load_grids():
	# Get a city grid file
	all_city_grids = [f for f in listdir(CITY_GRIDS_DIRECTORY) if (isfile(join(CITY_GRIDS_DIRECTORY, f)) and 'test' in f)]
	city_grid_file = join(CITY_GRIDS_DIRECTORY, random.choice(all_city_grids))
	
	# Get the corresponding SINR grid file
	index = city_grid_file.replace(CITY_GRIDS_DIRECTORY, '').replace('/train-city_grid', '').replace('/test-city_grid', '').replace('.txt', '')

	print(index)
	# Find the sinr grid file with the same index
	sinr_grid_file = join(SINR_GRIDS_DIRECTORY, [f for f in listdir(SINR_GRIDS_DIRECTORY) if 
		(isfile(join(SINR_GRIDS_DIRECTORY, f)) and (index == f.replace('train-sinr_grid', '').replace('test-sinr_grid', '').replace('.txt','')))][0])

	sinr_grid = np.loadtxt(sinr_grid_file, dtype=float)	
	city_grid = np.loadtxt(city_grid_file, dtype=float)			
	return sinr_grid, city_grid

def plot_grid(grid, title = ''):
	fig, ax1 = plt.subplots()

	masked_array = np.ma.array (grid, mask=np.isnan(grid))
	cmap = matplotlib.cm.plasma
	cmap.set_bad('white',1.)
	im1 = ax1.imshow(grid, cmap=cmap)
	ax1.imshow(masked_array, interpolation='nearest', cmap=cmap)
	ax1.set(xlabel='x (m)', ylabel='y (m)',
      title=title)
	ax1.set_xticks(np.arange(grid.shape[1], step = 50))
	ax1.set_xticklabels(np.arange(grid.shape[1], step = 50)*GRID_LEN)
	ax1.set_yticklabels(np.arange(grid.shape[0], step = 20)*GRID_LEN)
	ax1.set_yticks(np.arange(grid.shape[0], step = 20))
	cbar1 = ax1.figure.colorbar(im1, ax=ax1, cmap=cmap, fraction=0.046, pad=0.04)
	cbar1.ax.set_ylabel('Height (m)', rotation=-90, va="bottom")
	

def plot_nodes(building_grid, sinr_grid, nodes):
	fig, (ax1, ax2) = plt.subplots(2, 1)


	im1 = ax1.imshow(building_grid, cmap='Pastel2')
	ax1.scatter([n.coordinates[0] for n in nodes], [n.coordinates[1] for n in nodes], c='r', s=2)
	ax1.set(xlabel='x (m)', ylabel='y (m)',
      title='Trajectory')
	ax1.set_xticks(np.arange(building_grid.shape[1], step = 50))
	ax1.set_xticklabels(np.arange(building_grid.shape[1], step = 50)*GRID_LEN)
	ax1.set_yticklabels(np.arange(building_grid.shape[0], step = 20)*GRID_LEN)
	ax1.set_yticks(np.arange(building_grid.shape[0], step = 20))
	cbar1 = ax1.figure.colorbar(im1, ax=ax1, cmap='Pastel2', fraction=0.046, pad=0.04)
	cbar1.ax.set_ylabel('Height (m)', rotation=-90, va="bottom")
	
	im2 = ax2.imshow(sinr_grid, cmap='plasma')
	ax2.scatter([n.coordinates[0] for n in nodes], [n.coordinates[1] for n in nodes], c='r', s=2)
	ax2.set(xlabel='x (m)', ylabel='y (m)')
	ax2.set_xticks(np.arange(building_grid.shape[1], step = 50))
	ax2.set_xticklabels(np.arange(building_grid.shape[1], step = 50)*GRID_LEN)
	ax2.set_yticklabels(np.arange(building_grid.shape[0], step = 20)*GRID_LEN)
	ax2.set_yticks(np.arange(building_grid.shape[0], step = 20))
	cbar2 = ax2.figure.colorbar(im2, ax=ax2, cmap='plasma', fraction=0.046, pad=0.04)
	cbar2.ax.set_ylabel('SINR (dB)', rotation=-90, va="bottom")
	
