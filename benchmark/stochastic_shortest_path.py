from .utils import load_grids, plot_grid, plot_nodes
from .channel_reconstruction import reconstruct_channel
from environment import STOPPING_SINR, NOISE_LEVEL, UAV_ALTITUDE
import random 
import sys
import numpy as np
import  scipy.stats 
from os import listdir
import argparse
from os.path import isfile, join
import matplotlib.pyplot as plt
np.set_printoptions(threshold=sys.maxsize)

STOPPING_POWER = STOPPING_SINR + NOISE_LEVEL
print('Stopping power:', STOPPING_POWER)

seed = 1369
seed = random.randint(0, 9999)
print('random seed = %d' % seed)
random.seed(seed)

class Vertex:
	"""
	To solve the stochastic shortest path problem, we cast the space
	as a graph.
	"""
	def __init__(self, x, y, received_power, ground_user = False):
		self.coordinates = np.asarray([x, y])
		self.distances = np.asarray([])
		self.received_power = received_power
		self.ground_user = ground_user # True if this is the location of the receiver on the ground
		#self.starting_sinr = starting_sinr

class StoppingCondition:
	# If the value has been the same for n steps and lower than Inf
	# call stopping condition
	def __init__(self, max_repetitions):
		self.max_repetitions = max_repetitions
		self.repetitions = 0
		self.old_value = np.Inf
	def check_stoppping(self, value):
		done = False
		if value < np.Inf:
			if self.old_value != value:
				self.old_value = value
				self.repetitions = 1
			else:
				self.repetitions = self.repetitions + 1 

		return self.repetitions >= self.max_repetitions


def save_statistics(success_history, episode_lengths, is_shortest_path):
	success_history = np.asarray(success_history)
	episode_lengths = np.asarray(episode_lengths)
	if is_shortest_path:
		outfile = 'stats/'+'shortest_path'
	else:
		outfile = 'stats/'+'stochastic_shortest_path'	
	np.savez(outfile, success_history = success_history, episode_lengths = episode_lengths)
	print('Successfully stored the stats.')

def get_stochastic_shortest_path(power_grid, n_measured_points, test = False, render = False, is_shortest_path = False):
	y_size, x_size = power_grid.shape
	n_measured_points = int(y_size*x_size * n_measured_points / 100) # Convert the number of points from percentage to a value
	all_coordinates = np.concatenate([np.kron(np.arange(y_size).reshape((y_size, 1)), np.ones((x_size, 1))), np.kron(np.ones((y_size, 1)), np.arange(x_size).reshape((x_size, 1)))], axis = 1).astype(np.int)
	if is_shortest_path:
		probability_grid = np.ones(power_grid.shape)
	else:
		if test:
			# Get the probability of detection grid
			Gamma_r = power_grid # Initialize to this even though the reconstruct values will be repopulated
			Sigma_r = np.ones((y_size*x_size, x_size*y_size)) * 200 # Assign a small value from now
			
			Select_Sigma_r = np.zeros(Gamma_r.shape)
			Select_Sigma_r[all_coordinates[:, 0], all_coordinates[:, 1]] = Sigma_r[range(len(all_coordinates)), range(len(all_coordinates))]
		else:
			Gamma_r, Sigma_r, measured_coordinates, _ = reconstruct_channel(n_measured_points, power_grid)
			Select_Sigma_r = np.zeros(Gamma_r.shape)
			Select_Sigma_r[all_coordinates[:, 0], all_coordinates[:, 1]] = Sigma_r[range(len(all_coordinates)), range(len(all_coordinates))]
			reconstruct_grid = Gamma_r
			blacked_out_grid = np.empty(sinr_grid.shape)
			blacked_out_grid[:] = np.nan
			blacked_out_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]] = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]]
			if render:
				plot_grid(blacked_out_grid, title = 'Measured points')
				plot_grid(reconstruct_grid, title = 'Reconstruction values')
				plot_grid(power_grid, title = 'Actual values')
				plot_grid(sinr_grid, title = 'SINR grid')

		# Calculate the probability of connection at each point
		probability_grid = 1 - scipy.stats.norm().cdf((STOPPING_POWER - Gamma_r)/np.sqrt(Select_Sigma_r))

		if render:
			plot_grid(probability_grid, title = 'Probability grid')

	# Find the location of the user on the ground
	y_max, x_max = np.unravel_index(power_grid.argmax(), power_grid.shape)

	# Collect all the vertices
	starting_nodes = [] # Nodes with received power below threshold
	terminal_nodes = [] # Nodes with received power above threshold
	for xidx in range(x_size):
		for yidx in range(y_size):
			if city_grid[yidx, xidx] < UAV_ALTITUDE:	
				if power_grid[yidx, xidx] < STOPPING_POWER:		 
					starting_nodes.append(Vertex(xidx, yidx, power_grid[yidx, xidx]))
				else: 
					ground_user = False
					if (yidx == y_max) and (xidx == x_max):
						ground_user = True
					terminal_nodes.append(Vertex(xidx, yidx, power_grid[yidx, xidx], ground_user))

	all_nodes = starting_nodes + terminal_nodes

	# Pick a vertex t as a starting point
	t =  random.choice(all_nodes[0:len(starting_nodes)])

	# Impose the DAG requirement on all nodes
	new_all_nodes = []
	if x_max > t.coordinates[0]:
		x_range = (t.coordinates[0], x_max)
	else:
		x_range = (x_max, t.coordinates[0])
	if y_max > t.coordinates[1]:
		y_range = (t.coordinates[1], y_max)
	else:
		y_range = (y_max, t.coordinates[1])
	for v in all_nodes:
		if (v.coordinates[0] <= x_range[1]) and (v.coordinates[0] >= x_range[0]):
			if (v.coordinates[1] <= y_range[1]) and (v.coordinates[1] >= y_range[0]):
				new_all_nodes.append(v)
	all_nodes = new_all_nodes

	# Store the index of the destination
	index_of_ground_user_node = [v for v in range(len(all_nodes)) if all_nodes[v].ground_user][0]

	# First calculate one-step distances
	one_step_distances = np.zeros((len(all_nodes), len(all_nodes)))
	all_coordinates = np.asarray([v.coordinates for v in all_nodes])
	for i in range(len(all_nodes)):
		separation = (all_coordinates - all_coordinates[i,:])
		distances = np.sqrt(np.square(separation[:, 0]) + np.square(separation[:, 1]))
		distances = distances
		distances[distances > 1] = np.Inf 
		probabilistic_distances = distances
		epsilon = 1
		probabilistic_distances[probabilistic_distances <= 1] = probabilistic_distances[probabilistic_distances <= 1] * (1 - probability_grid[all_coordinates[i,1], all_coordinates[i,0]]) + epsilon
		one_step_distances[i, :] = probabilistic_distances

	# Find the shortest path
	stopping_cond = StoppingCondition(30)
	max_steps =  len(all_nodes) - 1
	t.distances = np.zeros((len(all_nodes), len(all_nodes)-1)) + np.Inf
	for i in range(max_steps):
		try:
			print('Progress %d / %d' % (i, max_steps))
			for j in range(len(all_nodes)):
				v = all_nodes[j]
				if v != t:
					if i != 0: 
						# Find i-1 step away nodes that is the closest
						distances = t.distances[:, i - 1] + one_step_distances[j, :]
						# Remove infinity distances
						distances = distances[ distances < np.Inf ]
						if len(distances):
							min_distance = np.min(distances)
						else:
							min_distance = np.Inf
						t.distances[j, i] = min_distance
				else:
					t.distances[j, i] = 0

			print('Shortest stochastic distance to the ground user: %f' % t.distances[index_of_ground_user_node, i])
			if stopping_cond.check_stoppping(t.distances[index_of_ground_user_node, i]):
				print('Stopping condition met.')
				break
		
		except KeyboardInterrupt:  
			i = i - 1
			print('Interrupted.')
			break
	shortest_path = []
	
	# Check if the algorithm was successful
	successful = t.distances[index_of_ground_user_node, i] < np.Inf

	# Retrieve the shortest path
	for k in range(i, -1, -1):
		if k == i:
			next_node = index_of_ground_user_node
			shortest_path.append(next_node)
		else:
			next_node = np.argmax((t.distances[: , k] + one_step_distances[next_node, :]) == t.distances[next_node , k + 1] )
			shortest_path.append(next_node)

		if all_nodes[next_node] == t:
			break
	nodes_on_the_shortest_path = [all_nodes[p] for p in shortest_path]
	nodes_on_the_shortest_path.reverse()

	return nodes_on_the_shortest_path, all_nodes, successful

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Run the benchmark approach.')
	parser.add_argument('--render', dest='render', 
	            action='store_true', help = "If included, the environment will be rendered.", default = False)
	parser.add_argument('--shortest-path', dest='shortest_path', 
	            action='store_true', help = "If included, only the shortest path towards will be saved.", default = False)
	parser.add_argument('--save', dest='save', 
	            action='store_true', help = "If included, the statistics will be saved.", default = False)
	args = parser.parse_args()
	render = args.render
	save_stats = args.save
	is_shortest_path = args.shortest_path

	# Store the results in these arrays
	episode_lengths = []
	success_history = []

	trials = 1000
	for z in range(trials):
		print('Trial %d' % (z+1))
		try:
			sinr_grid, city_grid = load_grids() # Load a random grid
			power_grid = sinr_grid + NOISE_LEVEL
			n_measured_points = 5 # percentage


			nodes_on_the_shortest_path, all_nodes, successful = get_stochastic_shortest_path(power_grid, n_measured_points, test = False, render = render, is_shortest_path = is_shortest_path)		
			
			if successful:
				# Find the distance traveled to the first connection point
				d = 0			
				for node in nodes_on_the_shortest_path:
					if node.received_power >= STOPPING_POWER:
						break
					d+=1
				print('Distance traveled to the first connected location: %d' % d)
				episode_lengths.append(d)

			if render:
				plot_nodes(city_grid, sinr_grid, nodes_on_the_shortest_path)
			
			plt.show()
			success_history.append(successful)
		except MemoryError:
			print('Caught a memory error.')
			continue
		except KeyboardInterrupt:    
			print('Interrupted.')
			break

	if save_stats:
		save_statistics(success_history, episode_lengths, is_shortest_path)
