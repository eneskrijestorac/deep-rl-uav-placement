from .utils import load_grids, plot_grid
from environment import NOISE_LEVEL, SINR_GRIDS_DIRECTORY, CITY_GRIDS_DIRECTORY, GRID_LEN, SINR_OF_AN_INACCESSIBLE_FIELD, UAV_ALTITUDE, USER_ALTITUDE
import random 
import sys
import numpy as np
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg') 




# These were obtained by running fit_model.py
sigma_sh = 19.634844
beta_sh = 44.909175
eta_pl = 3.229436
K = 74.251080
z = UAV_ALTITUDE - USER_ALTITUDE
reconst_chunk_size = 5000 # Number of points to be reconstructed in a single loop. This is for memory considerations


# Pick a random number of cooridnates that will be measured and used for reconstruction
n_measured_points = 1850

def reconstruct_channel(n_measured_points, power_grid):
	y_size, x_size = power_grid.shape

	Gamma_r = np.zeros(power_grid.shape) # Initialize to this even though the reconstruct values will be repopulated
	Sigma_r = np.zeros((y_size*x_size, x_size*y_size))

	# A list of all possible coordinates. Note that the coordinates are scaled by grid len
	all_coordinates = np.concatenate([np.kron(np.arange(y_size).reshape((y_size, 1)), np.ones((x_size, 1))), np.kron(np.ones((y_size, 1)), np.arange(x_size).reshape((x_size, 1)))], axis = 1)

	# Index of all unmeasured coordinates
	all_points = list(range(y_size * x_size)); random.shuffle(all_points)

	# Index of all measured points
	measured_points = all_points[0:n_measured_points]

	# All measured coordinates
	measured_coordinates = all_coordinates[measured_points, :].astype(np.int)

	# Split all the point for reconstruction into chunks
	all_chunks = list(range(n_measured_points, len(all_points), reconst_chunk_size))
	if all_chunks[-1] < len(all_points):
		all_chunks.append(len(all_points))


	# Calculate Phi_q
	Phi_q = np.kron(np.ones((1, n_measured_points, 1)), measured_coordinates.reshape((n_measured_points, 1, 2))) - \
		np.kron(np.ones((n_measured_points, 1, 1)), measured_coordinates.reshape((1, n_measured_points, 2)))
	Phi_q = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Phi_q, axis = 2))
	Phi_q_inv = np.linalg.inv(Phi_q)

	# Get gamma q
	Gamma_q = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]].reshape(n_measured_points, 1)



	# Calculate H_q
	D_q = 10 * np.log(np.linalg.norm(np.concatenate([measured_coordinates, np.ones((n_measured_points,1))* z] , axis = 1), axis = 1)).reshape((n_measured_points,1))
	H_q = np.concatenate([np.ones((n_measured_points, 1)), -D_q], axis = 1)


	# Calculate theta
	theta = np.asarray([K, eta_pl]).reshape((2, 1))

	start = n_measured_points
	for end in all_chunks[1:]:
		unmeasured_points = all_points[start :end]
		unmeasured_coordinates = all_coordinates[unmeasured_points, :].astype(np.int)
		n_unmeasured_points = end - start

		# Calculate Phi_r
		Phi_r = np.kron(np.ones((1, n_unmeasured_points, 1)), unmeasured_coordinates.reshape((n_unmeasured_points, 1, 2))) - \
			np.kron(np.ones((n_unmeasured_points, 1, 1)), unmeasured_coordinates.reshape((1, n_unmeasured_points, 2)))
		Phi_r = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Phi_r, axis = 2))

		# Calculate Psi_r_q
		Psi_r_q = np.kron(np.ones((1, n_measured_points, 1)), unmeasured_coordinates.reshape((n_unmeasured_points, 1, 2))) - \
						np.kron(np.ones((n_unmeasured_points, 1, 1)), measured_coordinates.reshape((1, n_measured_points, 2)))
		Psi_r_q = (sigma_sh ** 2) * np.exp((-GRID_LEN/beta_sh) * np.linalg.norm(Psi_r_q, axis = 2))

		
		# Calcaulate H_r
		D_r = 10 * np.log(np.linalg.norm(np.concatenate([unmeasured_coordinates, np.ones((n_unmeasured_points,1))* z] , axis = 1), axis = 1)).reshape((n_unmeasured_points,1))
		H_r = np.concatenate([np.ones((n_unmeasured_points, 1)), -D_r], axis = 1)

		# Calculate Gamma_r
		Psi_inv_dot_Phi = Psi_r_q.dot(Phi_q_inv)
		Gamma_r_temp = H_r.dot(theta) +  Psi_inv_dot_Phi.dot(Gamma_q - H_q.dot(theta))
		Sigma_r_temp = Phi_r - Psi_inv_dot_Phi.dot(Psi_r_q.T)
		
		Gamma_r[unmeasured_coordinates[:, 0], unmeasured_coordinates[:, 1]] = Gamma_r_temp.T
		Sigma_r[np.asarray(unmeasured_points)[:, None], np.asarray(unmeasured_points)[None, :]] = Sigma_r_temp

		start = end
	Gamma_r[measured_coordinates[:, 0], measured_coordinates[:, 1]] = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]]
	return Gamma_r, Sigma_r, measured_coordinates, all_coordinates


if __name__ == "__main__":
	sinr_grid, city_grid = load_grids() # Load grids
	power_grid = sinr_grid + NOISE_LEVEL

	Gamma_r, Sigma_r, measured_coordinates, _ = reconstruct_channel(n_measured_points, power_grid)


	reconstruct_grid = Gamma_r


	blacked_out_grid = np.empty(sinr_grid.shape)
	blacked_out_grid[:] = np.nan
	blacked_out_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]] = power_grid[measured_coordinates[:, 0], measured_coordinates[:, 1]]




	plot_grid(blacked_out_grid, title = 'Measured points')
	plot_grid(reconstruct_grid, title = 'Reconstruction values')
	plot_grid(power_grid, title = 'Actual values')
	plot_grid(sinr_grid, title = 'SINR grid')
	plt.show()