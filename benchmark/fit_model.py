"""
Try to fit a channel model into the data we got from the ray tracing simulator, so that we may get the parameters
needed for channel reconstruction
"""
from os import listdir
from os.path import isfile, join
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import random
from  process_ray_trac_dat import X_COORD_COL, Y_COORD_COL, Z_COORD_COL, SINR_COL, COMM_DATA_DIR, read_csv_data
from environment import NOISE_LEVEL, SINR_GRIDS_DIRECTORY, CITY_GRIDS_DIRECTORY, GRID_LEN, SINR_OF_AN_INACCESSIBLE_FIELD, UAV_ALTITUDE, USER_ALTITUDE
transmitter_height = 2
matplotlib.use('TkAgg') 
plt.rcParams.update({'font.size': 16})

def transform_coordinates(data):
	# Transform the cooridnates to a coordinate system with the transmitter at the center 

	# To find the location of the transmitter we find the coordinates with maximum SINR
	idx = np.argmax(data[:, 3])
	
	# Get the transmitter location
	transmitter_loc = data[idx, 0:3] - np.asarray([0, 0, data[idx, 2] - transmitter_height])
	
	# Shift the position coordinates
	data[:, 0:3] =  data[:, 0:3] - transmitter_loc


	return data

def add_a_distance_column(data):
	# Add a distance column as the first column to the data array
	new_data = np.zeros((data.shape[0], data.shape[1] + 1))
	distances = np.sqrt(np.square(data[:,0]) + np.square(data[:,1]) + np.square(data[:,2]))
	new_data[:, 1:] = data
	new_data[:, 0] = distances
	return new_data

def remove_dead_zones(data):
	new_data = data[data[:, 4] > SINR_OF_AN_INACCESSIBLE_FIELD]
	return new_data

def convert_to_power(data):
	data[:, 4] = data[:, 4] + NOISE_LEVEL
	return data

def main():
	all_comm_files = [f for f in listdir(COMM_DATA_DIR) if isfile(join(COMM_DATA_DIR, f))] 
	stacked_data = np.asarray([])
	# for comm_file in all_comm_files[7:8]:
	# 	comm_file_path = join(COMM_DATA_DIR, comm_file)

	# 	data = read_csv_data(comm_file_path)
	# 	data = transform_coordinates(data)
	# 	data = add_a_distance_column(data)
	# 	data = remove_dead_zones(data)
	# 	data = remove_noise_power(data)

	# 	if len(stacked_data):
	# 		stacked_data = np.concatenate((stacked_data, data), axis = 0)
	# 	else:
	# 		stacked_data = data

	comm_file = '/home/enesk/repos/deep-learning-2019/ray_tracing_sim/outputs/urban_landscape_ottawa.noise.t8_24.r5.p2m'
	comm_file_path = join(COMM_DATA_DIR, comm_file)

	data = read_csv_data(comm_file_path)
	data = transform_coordinates(data)
	data = add_a_distance_column(data)
	data = remove_dead_zones(data)
	data = convert_to_power(data)
	stacked_data = data

	# First we fit a path loss model
	# P = K - 10nlog(d)
	# We fit the least square line to find K and n
	# [K, N] = [1 -10nlog(d)] \ [P]  
	d = np.linalg.norm(stacked_data[:, 0:3], axis = 1)
	P = stacked_data[:,4]
	A = np.concatenate((-10 * np.log(d).reshape((stacked_data.shape[0], 1)), np.ones((stacked_data.shape[0], 1))), axis = 1)
	n, K = np.linalg.lstsq(A, P.reshape((stacked_data.shape[0], 1)), rcond=None)[0]

	# Plot the autocorrelation
	fig, ax = plt.subplots()
	ax.scatter(d, P, label = 'Data', marker = 'o', s = 2)
	ax.plot(np.unique(d), K - 10 * n * np.log(np.unique(d)), color = 'r', label = 'Fit')
	ax.set(xlabel='d (m)', ylabel='P (dBm)', title='')
	ax.legend()
	print('K: %f, n: %f' % (K, n))

	# To find large scale fading we subtract the path loss model
	stacked_data[:, 4] = stacked_data[:, 4] - (K - n * 10 * np.log(d))

    # Select a subset of the data for memory reasons
	stacked_data = stacked_data[random.sample(range(stacked_data.shape[0]), 10000)]
	n_measured_points = stacked_data.shape[0]
	measured_coordinates = stacked_data[:, 0:3]

	# Find powers and coordinates
	powers = stacked_data[:, 4]
	distances = np.kron(np.ones((1, n_measured_points, 1)), measured_coordinates.reshape((n_measured_points, 1, 3))) - \
				np.kron(np.ones((n_measured_points, 1, 1)), measured_coordinates.reshape((1, n_measured_points, 3)))
	distances = np.linalg.norm(distances, axis = 2).flatten()
	distances = np.round(distances, 0)
	power_products = np.kron(np.ones((1, n_measured_points)), powers.reshape((n_measured_points, 1))) * \
				np.kron(np.ones((n_measured_points, 1)), powers.reshape((1, n_measured_points)))
	power_products = power_products.flatten()


	# Find the autocorrelation or mean power products
	unique_distances = np.unique(distances)
	# Shorten the distances we include to get better line shape
	unique_distances = unique_distances[unique_distances < 200]
	mean_power_products = np.zeros(unique_distances.shape)
	for i in range(len(unique_distances)):
		unique_distance = unique_distances[i]
		mean_power_products[i] = np.mean(power_products[distances == unique_distance])

	# Do least squares to find sigma_sh, beta_sh
	unique_distances  = unique_distances[mean_power_products > 0 ]
	mean_power_products = mean_power_products[mean_power_products > 0 ]	
	ln_R = np.log(mean_power_products)

	A =  np.concatenate((unique_distances.reshape((unique_distances.shape[0], 1)), np.ones((unique_distances.shape[0], 1))), axis = 1)
	a, b = np.linalg.lstsq(A, ln_R.reshape((ln_R.shape[0], 1)), rcond=None)[0]
	beta_sh  = -(1/a)
	sigma_sh = np.exp(b/2)
	print('sigma_sh: %f, beta_sh: %f' % (sigma_sh, beta_sh))

	fig, ax = plt.subplots()
	ax.scatter(unique_distances, mean_power_products, marker = 'o', s = 2, label = 'Data')
	ax.plot(unique_distances, (sigma_sh**2) * np.exp(-unique_distances/beta_sh), color = 'r', label = 'Fit')
	ax.set(xlabel=r'$\Delta q (m)$', ylabel=r'$R(q) (m)$', title='')
	ax.legend()

	# # We select a line on the receiver grid to find the 
	# x = stacked_data[1000, 1]
	# stacked_data = stacked_data[stacked_data[:, 1] == x, :]

	# # Sort by y value
	# stacked_data = stacked_data[stacked_data[:,2].argsort()]

	# # Find the autocorrelation
	# R = np.correlate(stacked_data[:, 4], stacked_data[:, 4], mode = 'same')
	# print((len(R)-1)/2)
	# R = R[int((len(R)-1)/2):] # Select the correct span
	# delta_d = np.arange(R.size) * GRID_LEN

	# # Use least squares to find sigma_sh, beta_sh
	# #ln_R = np.log(R)
	# # Normalize the autocorrelation
	# R_norm = R / max(R)
	# # Plot the autocorrelation
	# fig, ax = plt.subplots()
	# ax.scatter(delta_d, R_norm)
	# ax.set(xlabel='d', ylabel='R(k*d)', title='')
	

	plt.show()


if __name__ == "__main__":
	main()



#fig, ax = plt.subplots()
#ax.scatter(stacked_data[:, 2], stacked_data[:, 4])
#ax.scatter(stacked_data[:, 2], K - n * 10 * np.log(stacked_data[:, 0]))
#ax.set(xlabel='log(d)', ylabel='P (dBm)', title='')
#plt.show()
