import pandas as pd
import numpy as np
from geometry import get_building_grid
import matplotlib.pyplot as plt
import seaborn as sns
from os import listdir
from os.path import isfile, join

# The column numbers for the data coming from Wireless Insite
X_COORD_COL = 1
Y_COORD_COL = 2
Z_COORD_COL = 3
SINR_COL = 9

CITY_FILE = 'ray_tracing_sim/ottawa.city'
COMM_DATA_DIR = 'ray_tracing_sim/outputs/'

def read_csv_data(filename, lineskip = 2):
	'''
	Inputs:
		filename 
		lineskip	defines the number of initial lines in the file to be skipped
	Outputs:
		data 	a Lx4 numpy array where L is the number of data points (sorted by x, y coordinates)
	'''

	print("Reading file: %s" % filename)
	file = open(filename)
	
	# Skip the header lines 
	for i in range(lineskip):
		file.readline()
	
	# Extract only the required columns
	df = pd.read_csv(file, sep = '  ', header = None, engine = 'python')
	
	df = df.sort_values(by = [1, 2])
	data = np.zeros((df.shape[0], 4))
	data[:,:] = df.loc[:, (X_COORD_COL, Y_COORD_COL, Z_COORD_COL, SINR_COL)] 
	return data

def convert_data_into_grid(data, spacing = 5):
	'''
	The data coming in has float coordinates, however we know it is in fact a rectangular grid
	'''
	#print(data)
	xvals = np.unique(data[:, 0])
	yvals = np.unique(data[:, 1])
	sinr_grid = np.zeros((yvals.size, xvals.size),)
	
	for j in range(xvals.size):
		for i in range(yvals.size):
			sinr = data[j*yvals.size + i, 3]
			sinr_grid[yvals.size - i - 1 , j] = sinr


	return sinr_grid

def main():
	all_comm_files = [f for f in listdir(COMM_DATA_DIR) if isfile(join(COMM_DATA_DIR, f))] 
	building_grid = []
	i = 0
	for comm_file in all_comm_files:
		comm_file_path = join(COMM_DATA_DIR, comm_file)
		print('Processing file', comm_file_path)
		data = read_csv_data(comm_file_path)
		sinr_grid = convert_data_into_grid(data)
		if not(len(building_grid)):
			building_grid = get_building_grid(data, CITY_FILE)

		# Now we need to find the location of the user
		indmax = np.argmax(sinr_grid)
		coord = np.unravel_index(indmax, sinr_grid.shape)
		division = 101
		overlap = 2
		if coord[1] < division:
			data_type = 'test'
			sinr_grid_half = sinr_grid[:, 0:division + overlap]
			building_grid_half = building_grid[:, 0:division + overlap]
		else:
			data_type = 'train'
			sinr_grid_half = sinr_grid[:, division - overlap:]
			building_grid_half = building_grid[:, division - overlap:]
		# Make rotations of the grids to have more data
		fig, (ax1, ax2) = plt.subplots(2, 1)
		ax1.imshow(building_grid_half, cmap='plasma')
		ax1.set(xlabel='x', ylabel='y',
          title=comm_file+'-'+data_type)
		ax2.imshow(sinr_grid_half, cmap='plasma')
		np.savetxt('stored_grids/sinr_grids/%s-sinr_grid%d.txt' % (data_type, i), sinr_grid_half, fmt='%f')
		np.savetxt('stored_grids/city_grids/%s-city_grid%d.txt' % (data_type, i), building_grid_half, fmt='%f')
		i += 1	

	print('Completed.')

	plt.show()
if __name__ == "__main__":
	main()


#ax = sns.heatmap(sinr_grid, linewidth=0.5)
#plt.show()