class State(object):
    def __init__(self, first_step = True):
        self.first_step = first_step
        self.at_the_starting_position = True
        self.ready_to_take_optimal_step = False
        self.explored_steps = []
        self.explored_sinr_increases = []

class BruteForce(object):
    """
    The brute force algorithm tests all 4 directions and reverts to the best one.
    """ 
    def __init__(self, env):
        self.env = env
        self.state = State()
        self.step_size = env.action_space.step_size
        self.possible_steps = [(0, self.step_size), (0, -self.step_size), (self.step_size, 0), (-self.step_size, 0)]

    def reset_env(self):
        self.env.reset()
        
    def get_next_step(self):
        if self.state.at_the_starting_position:
            if self.state.ready_to_take_optimal_step:
                # Means that we have explored all 3 or 4 directions and are at the starting position
                # Find the optimal directions
                if self.state.first_step:
                    optimal_step_index = self.state.explored_sinr_increases.index(max(self.state.explored_sinr_increases))
                    self.state.first_step =  False
                else:
                    # Do this to avoid coming back to the previous position
                    optimal_step_index = self.state.explored_sinr_increases.index(max(self.state.explored_sinr_increases[1:]))

                next_step = self.state.explored_steps[optimal_step_index]

            else:
                # Explore the steps that haven't been explored
                for step in self.possible_steps:
                    if not(step in self.state.explored_steps):
                        next_step = step

        else:
            # Revert back to the starting position
            last_step = self.state.explored_steps[-1]
            next_step = (-last_step[0], -last_step[1])

        return next_step   

    def runtime_step(self):

        step = self.get_next_step()
        sinr_increase, done = self.env.euclidian_step(step[0], step[1])

        if done:
            self.state = State()

        else: 

            if self.state.at_the_starting_position:
                self.state.explored_steps.append(step)
                self.state.explored_sinr_increases.append(sinr_increase)
                self.state.at_the_starting_position = False 

            else:
                self.state.at_the_starting_position = True

            if self.state.ready_to_take_optimal_step:
                # Means that we have taken the optimal step
                
                last_step = self.state.explored_steps[-1]
                last_increase = self.state.explored_sinr_increases[-1]
                self.state = State(first_step = False)
                self.state.explored_sinr_increases.append(-last_increase)
                self.state.explored_steps.append((-last_step[0], -last_step[1]))                

            
            elif all([(step in self.state.explored_steps) for step in self.possible_steps]):
                # Check if we have explored all possbile steps
                # Means that we have explored all of the possible 4 options
                self.state.ready_to_take_optimal_step = True

                # Check if the last step has been optimal. In that case, we do not need to walk back
                last_step = self.state.explored_steps[-1]
                last_increase = self.state.explored_sinr_increases[-1]
                max_increase = max(self.state.explored_sinr_increases)
                if last_increase == max_increase:
                    self.state = State(first_step = False)
                    self.state.explored_sinr_increases.append(-last_increase)
                    self.state.explored_steps.append((-last_step[0], -last_step[1]))


        return done







