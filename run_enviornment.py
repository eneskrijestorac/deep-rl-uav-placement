import time
from environment import Environment
import matplotlib.pyplot as plt
import matplotlib 
matplotlib.use('TkAgg') 
import random




def run_random_policy():
	for i in range(100000):
		if i>0 and i % 128 == 0:
			env.render(include_obs = True)
			plt.pause(1000000)
		next_obs, reward, done, _ = env.step(env.get_random_action())
		if done:
			print('Episode done. Length: %d' % len(env.state_history))
			print('Final SINR: %f' % env.get_current_sinr())
			env.reset()

if __name__ == "__main__":	
	env = Environment(seed = random.randint(0, 33213), repeat_probability = 0.0)
	run_random_policy()