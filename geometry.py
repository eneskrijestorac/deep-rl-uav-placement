from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
from descartes import PolygonPatch

class Face:
	def __init__(self, nvertices):
		self.nvertices = nvertices
		self.vertices = []

	def __str__(self):
		s = 'Face\n'
		s += 'Number of vertices: ' + str(self.nvertices) + '\n'
		s += 'Horizontal: ' + str(self.is_horziontal()) + '\n'
		for i in range(len(self.vertices)):
			vertex = self.vertices[i]
			s += 'Vertex %d: %f, %f, %f\n' % (i, vertex[0], vertex[1], vertex[2])
		return(s)

	def add_vertex(self, vertex):
		'''
		Vertex is a list containing the x, y and z coordinate
		'''
		self.vertices.append(vertex) 

	def is_horziontal(self):
		zvals = [vertex[2] for vertex in self.vertices]
		return zvals.count(zvals[0]) == len(zvals)


def collect_faces(filename):
	'''
	Collects all the faces in the .city file provided by
	Wireless InSite
	'''
	begin_face_tag = 'begin_<face>'
	end_face_tag = 'end_<face>'
	file = open(filename)
	faces= []

	counter = 0
	for line in file:
		if begin_face_tag in line:
			counter = 1
		elif end_face_tag in line:
			faces.append(f)
			counter = 0
		elif counter == 2:
			n_vertices = [int(s) for s in line.split() if s.isdigit()][0]
			f = Face(n_vertices)
			counter += 1
		elif counter > 2:
			vertex = [float(s) for s in line.split()]
			f.add_vertex(tuple(vertex))
			counter += 1
		elif counter > 0:
			counter += 1

	return faces

def display_horizontal_faces(faces):
	# DOESN'T WORK AS OF NOW
	raise ImplementationError('Not implemnted!')
	patches = []
	# Create polygons from the horizontal faces
	for face in faces:
		if face.is_horziontal():
			#print([vertex[0:2] for vertex in face.vertices])
			p = Polygon([vertex[0:2] for vertex in face.vertices])
			patches.append(PolygonPatch(p, color = '#999999'))
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.add_collection(PatchCollection(patches))
	plt.title("Shapefile polygons rendered using Shapely")
	plt.tight_layout()
	plt.show()

def get_horizontal_faces_as_polygons(faces):
	polygons = []
	# Create polygons from the horizontal faces
	for face in faces:
		if face.is_horziontal():
			#print([vertex[0:3] for vertex in face.vertices])
			p = Polygon([vertex[0:3] for vertex in face.vertices])
			polygons.append(p)

	return polygons



def print_faces(faces):
	for face in faces:
		print(face)


def get_building_grid(comm_data, city_file):
	'''
	Uses the comm_data to construct the vertices for the grid 
	and then uses the polygons to trace out the building shapes
	on this grid
	'''
	print('Reading file:', city_file)

	xvals = np.unique(comm_data[:, 0])
	yvals = np.unique(comm_data[:, 1])
	building_grid = np.zeros((yvals.size, xvals.size),)

	# Get building faces
	faces = collect_faces(city_file)

	# Get horizontal faces
	polygons = get_horizontal_faces_as_polygons(faces)
	iter = 0
	for j in range(xvals.size):
		for i in range(yvals.size):
			print('%d/%d points'%(iter, building_grid.size), end="\r")
			x = xvals[j]
			y = yvals[i]
			z = 0
			for poly in polygons:
				if poly.contains(Point(x, y, poly.exterior.coords[0][2])):
					z = poly.exterior.coords[0][2]
					break
			building_grid[yvals.size - i - 1 , j] = z
			iter += 1
	print('%d/%d points'%(iter, building_grid.size), end="\r")	
	print('\n')		
	return building_grid
