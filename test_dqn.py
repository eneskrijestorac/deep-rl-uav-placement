import time
from environment import Environment
import matplotlib.pyplot as plt
import matplotlib 
from dqn_utils import PiecewiseSchedule

import random
import tensorflow as tf
from train_dqn import models, get_available_gpus, set_global_seeds, get_env, get_test_env, get_session, dqn_deep_model
import dqn
from benchmark_algs import BruteForce
import datetime
import argparse
import os
import numpy as np
matplotlib.use('TkAgg') 
plt.rcParams.update({'font.size': 20})


# Stopping criterion
def stopping_criterion(env, t):
    # notice that here t is the number of steps of the wrapped env,
    # which is different from the number of steps in the underlying env
    return env.get_total_steps() >= num_timesteps


def test(seed, num_timesteps, render, render_obs, include_benchmark, model_checkpoint, save_stats, stats_file, blind):
    # Give a name to this session
    session_name = "test-" + datetime.datetime.now().replace(microsecond=0).isoformat()    




    # Define parameters
    model = 'new_atari_model'
    hsize = 512 # Size of the hidden layer if an RNN is used
    double_q = True
    n_uavs = 1
    n_users = 1
    batch_norm = True
    rnn_model = models[model]['rnn_model']
    max_episode_length = 500
    assignment_update_freq = 1
    blind =  blind



    assignment = 'sinr'
    # action_randomness = 0.01, 
    # action_randomness = 0.02, 0.798
    # action_randomness = 0.03,  0.778
    # action_randomness = 0.04, 0.790
    # action_randomness = 0.05, 0.745
    action_randomness = 0.1

    # Get environment and tf session
    env = get_test_env(seed, max_episode_length, n_uavs = n_uavs, n_users = n_users, assignment = assignment, blind = blind)
    session = get_session()


    alg = dqn.QLearner(
        env=env,
        q_func=models[model]['graph'],
        session=session,
        session_name = session_name,
        stopping_criterion=stopping_criterion,
        double_q = double_q,
        rnn_model = rnn_model,
        batch_norm = batch_norm,
        model_checkpoint = model_checkpoint,
        hsize = hsize,
        runtime = True,
    )

    episode_count = 0
    step_count = 0
    completed_uavs = np.zeros(n_uavs, dtype = np.int)
    while 1:
        try:
            if step_count % assignment_update_freq == 0:
                if assignment == 'sinr':
                    q_t = np.zeros((n_uavs, n_users))
                    env.make_sinr_based_assignment(scale = 1, offset = q_t)
                elif assignment == 'qvalue':
                    obs = env.get_observations_for_all_users()
                    q_t = np.max(alg.get_q_t(obs), axis = 1).reshape(n_uavs, n_users)
                    env.make_sinr_based_assignment(scale = 0, offset = q_t)  
                elif assignment == 'mixed':
                    obs = env.get_observations_for_all_users()
                    q_t = np.max(alg.get_q_t(obs), axis = 1).reshape(n_uavs, n_users)
                    env.make_sinr_based_assignment(scale = 5, offset = q_t)                   
                elif assignment == 'random':
                    env.make_random_assignment()
                elif assignment == 'distance':
                    env.make_distance_based_assignment()
                else:
                    raise ValueError()

            for uav_idx in range(n_uavs):             
                if not(completed_uavs[uav_idx]):
                    done = alg.runtime_step(action_randomness, uav_idx = uav_idx)
                    if done:
                        completed_uavs[uav_idx] = 1


            if all(completed_uavs == 1):
                episode_count += 1
                for uav_idx in range(n_uavs):    
                    sinr_improvement = env.get_episode_sinr_improvement(uav_idx)
                    ep_len = env.get_episode_length(uav_idx)
                    success = env.get_episode_success(uav_idx)
                    if episode_count > 1:
                        sinr_improvements = env.get_sinr_improvements()  
                        success_history = env.get_success_history()
                        episode_lengths = env.get_episode_lengths()
                        mean_sinr_improvement = np.mean(sinr_improvements[:, uav_idx])
                        mean_success = np.mean(success_history[:, uav_idx])
                        mean_episode_length = np.mean(episode_lengths[:, uav_idx][success_history[:, uav_idx] == 1])
                    else:
                        mean_success = 0
                        mean_sinr_improvement = 0     
                        mean_episode_length = np.Inf

                    print("UAV: %2d, Ep. count: %4d. SINR increase: %+8.3f. Last ep. len: %4d, Success: %1d, Mean SINR increase: %+7.3f, Mean success rate: %1.4f, Mean episode len.: %5.2f" % 
                        (uav_idx, episode_count, sinr_improvement, ep_len, success, mean_sinr_improvement, mean_success, mean_episode_length) )

                    if render:
                        env.render(uav_idx)
                    if render_obs:
                        env.render_current_obs(0)
                if episode_count > 1 and n_uavs >1:
                    print("Overall, Ep. count: %4d. Mean SINR increase: %5.3f, Mean success rate: %1.4f, Mean episode len.: %5.2f" % 
                    (episode_count, np.mean(sinr_improvements[:]), np.mean(success_history[:]), np.mean(episode_lengths[success_history == 1][:])) )
                print('---------------------------------------------------------------------------------------------------------------------------------------')
                alg.reset_env()
                step_count = 0
                completed_uavs = np.zeros(n_uavs, dtype = np.int)
                if episode_count >= 1000:
                    print("Reached the set number of episodes.")
                    break 
            
            step_count += 1
        except KeyboardInterrupt:    
            print('Interrupted.')
            break

    env.close()




    if save_stats:
        save_statistics(env.get_sinr_improvements(), env.get_episode_rewards(),
        env.get_success_history(), env.get_episode_lengths(),session_name, model, assignment, assignment_update_freq, blind)





def plot_statistics_from_file(file):
    f = np.load(file)
    plot_statistics(f["sinr_history"], f["reward_history"], f["success_history"], f["episode_lengths"])
    
def save_statistics(sinr_history, reward_history, success_history, episode_lengths, session_name, model, assignment, assignment_update_freq, blind):
    if blind:
        blind = 'blind'
        outfile = 'stats/'+'dqn-%s-%s-%s-%d' % (model, blind, assignment, assignment_update_freq)
    else:
        outfile = 'stats/'+'dqn-%s-%s-%d' % (model, assignment, assignment_update_freq)
    
    np.savez(outfile, sinr_history = sinr_history, reward_history = reward_history, success_history = success_history, episode_lengths = episode_lengths)
    print('Successfully stored the stats.')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test the DQN model.')
    parser.add_argument('--model-checkpoint', dest='model_checkpoint', 
                action='store', help = "If included, the model will be initialized from this file.", default = "")
    parser.add_argument('--render', dest='render', 
                action='store_true', help = "If included, the environment will be rendered.", default = False)
    parser.add_argument('--renderobs', dest='renderobs', 
                action='store_true', help = "If included, the observations of the UAV will be rendered.", default = False)
    parser.add_argument('--save', dest='save', 
                action='store_true', help = "If included, the statistics will be saved.", default = False)
    parser.add_argument('--load-from-file', dest='stats_file', 
                action='store', help = "If included, the simulation will be skipped and statistics will be plotted from a file.", default = "")
    parser.add_argument('--include-benchmark', dest='include_benchmark', 
                action='store_true', help = "If included, statistics for a benchmark will be done too.", default = False)
    parser.add_argument('--blind', dest='blind', 
                action='store_true', help = "SINR only observations.", default = False)
    args = parser.parse_args()
    
    os.environ['CUDA_VISIBLE_DEVICES'] = '' # We don't need a GPU to test the model

    model_checkpoint = args.model_checkpoint
    render = args.render
    render_obs = args.renderobs
    save_stats = args.save
    stats_file = args.stats_file
    include_benchmark = args.include_benchmark
    blind = args.blind
    seed = 4273
    #seed = random.randint(0, 9999)
    print('random seed = %d' % seed)
    
    test(seed, num_timesteps=1e8, render = render, render_obs = render_obs, include_benchmark = include_benchmark, 
        model_checkpoint = model_checkpoint, save_stats = save_stats, stats_file = stats_file, blind = blind)
