#!/bin/csh -f
#  hoffman.sh.cmd
#
#  UGE job for hoffman.sh built Fri Apr  5 18:54:42 PDT 2019
#
#  The following items pertain to this script
#  Use current working directory
#$ -cwd
#  input           = /dev/null
#  output          = /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID
#$ -o /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID
#  error           = Merged with joblog
#$ -j y
#  The following items pertain to the user program
#  user program    = /u/home/e/enesk/deep-learning-2019/hoffman.sh
#  arguments       = 
#  program input   = Specified by user program
#  program output  = Specified by user program
#  Resources requested
#
#$ -l exclusive,num_proc=8,gpu,fermi,h_data=16384M,h_rt=24:00:00
# #
#  Name of application for log
#$ -v QQAPP=job
#  Email address to notify
#$ -M enesk@g.ucla.edu
#  Notify at beginning and end of job
#$ -m bea
#  Job is not rerunable
#$ -r n
#
# Initialization for serial execution
#
  unalias *
  set qqversion = 
  set qqapp     = "job serial"
  set qqidir    = /u/home/e/enesk/deep-learning-2019
  set qqjob     = hoffman.sh
  set qqodir    = /u/home/e/enesk/deep-learning-2019/outputs
  cd     /u/home/e/enesk/deep-learning-2019
  source /u/local/bin/qq.sge/qr.runtime
  source /u/local/Modules/default/init/modules.csh 
  module load cuda/7.0
  module load python/anaconda3  
  if ($status != 0) exit (1)
#
  echo "UGE job for hoffman.sh built Fri Apr  5 18:54:42 PDT 2019"
  echo ""
  echo "  hoffman.sh directory:"
  echo "    "/u/home/e/enesk/deep-learning-2019
  echo "  Submitted to UGE:"
  echo "    "$qqsubmit
  echo "  SCRATCH directory:"
  echo "    "$qqscratch
#
  echo ""
  echo "hoffman.sh started on:   "` hostname -s `
  echo "hoffman.sh started at:   "` date `
  echo ""
#
# Run the user program
#
  source /u/local/Modules/default/init/modules.csh
  module load intel/13.cs
#
  echo hoffman.sh "" \>\& hoffman.sh.output.$JOB_ID
  echo ""
  /usr/bin/time /u/home/e/enesk/deep-learning-2019/hoffman.sh  >& /u/home/e/enesk/deep-learning-2019/hoffman.sh.output.$JOB_ID
#
  echo ""
  echo "hoffman.sh finished at:  "` date `
#
# Cleanup after serial execution
#
  source /u/local/bin/qq.sge/qr.runtime
#
  echo "-------- /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID --------" >> /u/local/apps/queue.logs/job.log.serial
  if (`wc -l /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID  | awk '{print $1}'` >= 1000) then
	head -50 /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID >> /u/local/apps/queue.logs/job.log.serial
	echo " "  >> /u/local/apps/queue.logs/job.log.serial
	tail -10 /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID >> /u/local/apps/queue.logs/job.log.serial
  else
	cat /u/home/e/enesk/deep-learning-2019/hoffman.sh.joblog.$JOB_ID >> /u/local/apps/queue.logs/job.log.serial
  endif
  exit (0)
