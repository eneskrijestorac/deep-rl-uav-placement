import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from matplotlib.ticker import MaxNLocator
from environment import Environment
import matplotlib
import seaborn as sns
plt.rcParams.update({'font.size': 15})
matplotlib.use('TkAgg') 



def adjustFigAspect(fig,aspect=1):
    '''
    Adjust the subplot parameters so that the figure has the correct
    aspect ratio.
    '''
    xsize,ysize = fig.get_size_inches()
    minsize = min(xsize,ysize)
    xlim = .4*minsize/xsize
    ylim = .4*minsize/ysize
    if aspect < 1:
        xlim *= aspect
    else:
        ylim /= aspect
    fig.subplots_adjust(left=.5-xlim,
                        right=.5+xlim,
                        bottom=.5-ylim,
                        top=.5+ylim)


def plot_different_models(proposed, blind, max_av_increase, hours):
    epoch_length = 60
    smoothing_chunk = 51
    data1 = pd.read_csv(proposed, sep = ', ', engine='python')
    data2 = pd.read_csv(blind, sep = ', ', engine='python')


    select = data1['running_time'] <= (hours*60) 
    fig, ax = plt.subplots()
    #ax.plot(data1['running_time'][select]/epoch_length, savgol_filter(data1['mean_sinr_improvement'][select], smoothing_chunk, 1))
    ax.plot(data1['running_time'][select]/epoch_length, data1['mean_sinr_improvement'][select])
    #ax.plot(data2['running_time'][select]/epoch_length, savgol_filter(data2['mean_sinr_improvement'][select], smoothing_chunk, 1))
    select = data2['running_time'] <= (hours*60) 
    ax.plot(data2['running_time'][select]/epoch_length, data2['mean_sinr_improvement'][select])
    ax.plot(data2['running_time'][select]/epoch_length, np.ones(data2['timestep'][select].shape) * max_av_increase, linewidth=2)
    ax.set(xlabel='Training time (hrs)', ylabel='Average SINR increase',
           title='')
    ax.grid()
    ax.legend(['Proposed approach', 'Blind', 'Upper bound'])
    adjustFigAspect(fig,aspect=1.5)
    fig.savefig('plots/figures/training-comparison.png', pad_inches=0.1, bbox_inches = 'tight')

def plot_log_file(file, max_av_increase, reward = False, title = ''):
    # Compare LSTM and no LSTM
    epoch_length = 60
    smoothing_chunk = 1
    data1 = pd.read_csv(file, sep = ', ', engine='python')
    timesteps = len(data1['timestep'])
    fig, ax = plt.subplots()
    #ax.plot(data1['running_time'][0 : timesteps]/epoch_length, savgol_filter(data1['mean_sinr_improvement'][0 : timesteps], smoothing_chunk, 1), linewidth=2)
    ax.plot(data1['running_time'][0 : timesteps]/epoch_length, data1['mean_sinr_improvement'][0 : timesteps], linewidth=2)
    ax.plot(data1['running_time'][0 : timesteps]/epoch_length, np.ones(data1['timestep'][0 : timesteps].shape) * max_av_increase, linewidth=2)
    ax.set(xlabel='Training time (hrs)', ylabel='Average SINR increase',
           title=title)
    ax.legend(['DQN model increase', 'Maximum expected increase'])
    ax.grid()
    if reward:
        fig, ax = plt.subplots()
        ax.plot(data1['running_time'][0 : timesteps]/epoch_length, savgol_filter(data1['mean_reward'][0 : timesteps], smoothing_chunk, 1))
        ax.set(xlabel='Training steps (1e6)', ylabel='Average reward',
               title=title)
        ax.grid()

def plot_benchmark(stochastic, shortest_path):
    f = np.load(stochastic)
    episode_lengths = f["episode_lengths"]
    

    # Plot episode length histogram
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    bins = np.linspace(-10, 200, 100)
    ax.hist(episode_lengths.astype(np.int), alpha=0.5, bins=bins)
    ax.set(xlabel='Steps made until connectivity', ylabel='Count')
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.axvline(episode_lengths.astype(np.int).mean(), color='r', linestyle='dashed', linewidth=1)

    f = np.load(shortest_path)
    episode_lengths = f["episode_lengths"]

    ax.hist(episode_lengths.astype(np.int), alpha=0.5, bins=bins)
    ax.axvline(episode_lengths.astype(np.int).mean(), color='g', linestyle='dashed', linewidth=1)
    ax.legend(['Stochastic shortest path mean', 'Direct path to the remote stat. mean', 'Stochastic shortest path', 'Direct path to the remote stat.'])

def plot_model_cdf(proposed, blind, benchmark, title):
    f = np.load(proposed)
    success_history = f["success_history"].flatten()
    episode_lengths = f["episode_lengths"][success_history==1].flatten()

    fig, ax = plt.subplots(1, 1)
    bins = np.linspace(-10, 510, 100)
    ax.hist(episode_lengths, bins=bins, density=True, histtype='step',
                           cumulative=True, linewidth=3, label = 'Proposed (%.2f success rate)' % np.mean(success_history))

    f = np.load(blind)
    success_history = f["success_history"].flatten()
    episode_lengths = f["episode_lengths"][success_history==1].flatten()
    ax.hist(episode_lengths, bins=bins, density=True, histtype='step', 
                           cumulative=True, linewidth=3, label = 'Blind (%.2f success rate)' % np.mean(success_history))

    f = np.load(benchmark)
    success_history = f["success_history"].flatten()
    episode_lengths = f["episode_lengths"].flatten()
    ax.hist(episode_lengths, bins=bins, density=True, histtype='step',
                           cumulative=True, linewidth=3, label = "Muralidharan et al.")

    ax.set(xlabel='Average number of steps per UAV\n(4 m step size)', ylabel='CDF')
    ax.set_xlim([-20,498])
    ax.legend(loc = 'lower right')
    fig.savefig('plots/figures/cdf.png', pad_inches=0.1, bbox_inches = 'tight')
def plot_model_statistics_box(sinr_assoc_file, random_assoc_file, qval_assoc_file, title):
    labels = ['SINR assoc.\nSuccess prob: %.2f', 'Random assoc.\nSuccess prob: %.2f', 'Distance assoc.\nSuccess prob: %.2f']

    episode_lengths = []
    success_histories = []
    f = np.load(sinr_assoc_file)
    success_history = f["success_history"].flatten()
    success_histories.append(success_history)
    episode_lengths.append(f["episode_lengths"].flatten()[success_history==1])
    labels[0] = labels[0] % (np.mean(success_history))

    f = np.load(random_assoc_file)
    success_history = f["success_history"].flatten()
    success_histories.append(success_history)
    episode_lengths.append(f["episode_lengths"].flatten()[success_history==1])
    labels[1] = labels[1] % (np.mean(success_history))   

    f = np.load(qval_assoc_file)
    success_history = f["success_history"].flatten()
    success_histories.append(success_history)
    episode_lengths.append(f["episode_lengths"].flatten()[success_history==1])
    labels[2] = labels[2] % (np.mean(success_history))

    fig, ax1 = plt.subplots()
    ax1.boxplot(episode_lengths, whis = 0.7, showmeans=True, showfliers=False, labels = labels, positions = [0, 3.5, 7], widths = 0.75)
    ax1.set(title = title, ylabel = 'Number of iterations')
    ax1.yaxis.grid()

def plot_model_statistics(file, max_av_sinr_increase, benchmark_file = False):

    f = np.load(file)
    sinr_history = f["sinr_history"]
    reward_history = f["reward_history"]
    success_history = f["success_history"]
    episode_lengths = f["episode_lengths"]

    if benchmark_file:
        f = np.load(benchmark_file)
        ben_success_history = f["success_history"]
        ben_episode_lengths = f["episode_lengths"]

    # Plot SINR histogram
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    n_bins = 200
    ax.hist(sinr_history, bins=n_bins)
    ax.set(xlabel='SINR improvement (dB)', ylabel='Count')
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.axvline(sinr_history.mean(), color='r', linestyle='dashed', linewidth=1)
    ax.axvline(max_av_sinr_increase, color='g', linestyle='dashed', linewidth=1)
    ax.set_xlim(-50, 150)
    fig.savefig('plots/figures/sinr_hist.png')
    ax.legend(['Mean', 'Upper bound', 'Distribution'])

    # Plot success histogram
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    n_bins = 100
    ax.hist(success_history.astype(np.int), density = True, bins=n_bins)
    ax.set(xlabel='', ylabel='Convergence success (%)')
    ax.set_xticks([-0.05, 0.995])
    ax.set_xticklabels(['False', 'True'])
    #ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    #ax.axvline(success_history.astype(np.int).mean(), color='r', linestyle='dashed', linewidth=1)
    fig.savefig('plots/figures/success_hist.png')


    # Plot episode length histogram
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    n_bins = 20
    ax.hist(episode_lengths.astype(np.int), bins=n_bins)
    
    ax.set(xlabel='Episode lengths', ylabel='Count')

    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.axvline(episode_lengths.astype(np.int).mean(), color='r', linestyle='dashed', linewidth=1)
    fig.savefig('plots/figures/episodes_hist.png')

    # Plot successful episode lengths
    successful_episodes = success_history == 1
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    bins = np.linspace(-10, 1010, 100)
    
    if benchmark_file:
        ax.hist(episode_lengths[successful_episodes].astype(np.int), alpha=0.5, bins=bins, density = True)
        ax.hist(ben_episode_lengths.astype(np.int), alpha=0.5, bins=bins, density = True)
        ax.axvline(ben_episode_lengths.astype(np.int).mean(), color='g', linestyle='dashed', linewidth=1)
        ax.axvline(episode_lengths[successful_episodes].astype(np.int).mean(), color='r', linestyle='dashed', linewidth=1)
        ax.legend(['Proposed approach mean', 'Benchmark mean', 'Proposed approach', 'Benchmark', ])
        
    else:
        ax.hist(episode_lengths[successful_episodes].astype(np.int), bins=bins)
        ax.axvline(episode_lengths[successful_episodes].astype(np.int).mean(), color='r', linestyle='dashed', linewidth=1)
        ax.legend(['Mean', 'Distribution'])
    ax.set(xlabel='Successful episode lengths', ylabel='Count')
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
   
    
    
    fig.savefig('plots/figures/success_episodes_hist.png')



if __name__ == "__main__":
    #epoch_length = 10000
    Environment.get_maximum_expected_sinr_increase()
    max_av_sinr_increase = np.load('stats/expected_sinr_increase.npz')['train']
    #plot_different_models(max_av_sinr_increase)
    #plot_benchmark('/home/enesk/repos/deep-learning-2019/stats/benchmark.npz')
    #plot_model_statistics('/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model.npz', max_av_sinr_increase, benchmark_file = '/home/enesk/repos/deep-learning-2019/stats/benchmark.npz')
    #plot_model_statistics('/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model.npz', max_av_sinr_increase)
    plot_log_file('/home/enesk/repos/deep-learning-2019/logs/logging-2019-06-19T00_49_45.log', max_av_sinr_increase)
    plot_log_file('/home/enesk/repos/deep-learning-2019/logs/logging-2019-06-20T15:06:09.log', max_av_sinr_increase)

    plot_different_models('/home/enesk/repos/deep-learning-2019/logs/logging-2019-06-18T16:24:27.log', 
        '/home/enesk/repos/deep-learning-2019/logs/logging-2019-06-19T00_49_45.log', max_av_sinr_increase, 8)

    #plot_benchmark('/home/enesk/repos/deep-learning-2019/stats/stochastic_shortest_path.npz', '/home/enesk/repos/deep-learning-2019/stats/shortest_path.npz')

    #plot_model_statistics_box('/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-sinr-1.npz', 
    #    '/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-random-1000.npz', '/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-distance-1.npz', '')
    #plot_model_statistics_box('/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-sinr-10.npz', '/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-random-10.npz', '10 step update')
    plot_model_cdf('/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-sinr-1.npz', 
        '/home/enesk/repos/deep-learning-2019/stats/dqn-new_atari_model-blind-sinr-1.npz', '/home/enesk/repos/deep-learning-2019/stats/stochastic_shortest_path.npz', '')
    plt.show()

