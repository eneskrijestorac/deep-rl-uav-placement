import uuid
import time
import sys
import itertools
import numpy as np
import random
import tensorflow                as tf
import tensorflow.contrib.layers as layers
import pickle
from collections import namedtuple
from dqn_utils import *

import matplotlib
matplotlib.use('Agg') 
import matplotlib.pyplot as plt

OptimizerSpec = namedtuple("OptimizerSpec", ["constructor", "kwargs", "lr_schedule"])

class QLearner(object):
    def __init__(
        self,     
        env,
        q_func,  
        session,
        checkpoint_interval = 1000000000,
        session_name = 'test',
        optimizer_spec = None,
        exploration=LinearSchedule(1000000, 0.1),
        stopping_criterion=None,
        replay_buffer_size=60000,
        batch_size=32,
        hsize  = 512,
        gamma=0.99,
        model_checkpoint = "",
        start_t = 0,
        learning_starts=50000,
        learning_freq=4,
        frame_history_len=1,
        target_update_freq=10000,
        grad_norm_clipping=10,
        rew_file=None,
        trace_size = 9,
        rnn_model =  False,
        double_q=True,
        runtime = False,
        dropout = 0.0,
        batch_norm = False,
        prioritized_replay = False,
        n_uavs = 1):
        """Deep Q-learning algorithm.

        You can specify your own convnet using q_func.

        All schedules are w.r.t. total number of steps taken in the environment.

        Parameters
        ----------
        env:  environment to train on.
        q_func: function
            Model to use for computing the q function. It should accept the
            following named arguments:
                img_in: tf.Tensor
                    tensorflow tensor representing the input 
                num_actions: int
                    number of actions
                scope: str
                    scope in which all the model related variables
                    should be created
                reuse: bool
                    whether previously created variables should be reused.
        optimizer_spec: OptimizerSpec
            Specifying the constructor and kwargs, as well as learning rate schedule
            for the optimizer
        session: tf.Session
            tensorflow session to use.
        exploration: rl_algs.deepq.utils.schedules.Schedule
            schedule for probability of chosing random action.
        stopping_criterion: (env, t) -> bool
            should return true when it's ok for the RL algorithm to stop.
            takes in env and the number of steps executed so far.
        replay_buffer_size: int
            How many memories to store in the replay buffer.
        batch_size: int
            How many transitions to sample each time experience is replayed.
        gamma: float
            Discount Factor
        learning_starts: int
            After how many environment steps to start replaying experiences
        learning_freq: int
            How many steps of environment to take between every experience replay
        frame_history_len: int
            How many past frames to include as input to the model.
        target_update_freq: int
            How many experience replay rounds (not steps!) to perform between
            each update to the target Q network
        grad_norm_clipping: float or None
            If not None gradients' norms are clipped to this value.
        double_q: bool
            If True, then use double Q-learning to compute target values. Otherwise, use vanilla DQN.
            https://papers.nips.cc/paper/3964-double-q-learning.pdf
        """
        #assert type(env.observation_space) == gym.spaces.Box
        #assert type(env.action_space)      == gym.spaces.Discrete

        # Create a logging file

        self.target_update_freq = target_update_freq
        self.optimizer_spec = optimizer_spec
        self.batch_size = batch_size
        self.trace_size = trace_size
        self.runtime = runtime
        self.hsize = hsize
        self.learning_freq = learning_freq
        self.learning_starts = learning_starts
        self.stopping_criterion = stopping_criterion
        self.env = env
        self.dropout = dropout
        self.batch_norm = batch_norm
        self.rnn_model = rnn_model
        self.session = session
        self.exploration = exploration
        self.session_name = session_name
        self.rew_file = str(uuid.uuid4()) + '.pkl' if rew_file is None else rew_file
        self.double_q = double_q
        self.prioritized_replay = prioritized_replay
        self.checkpoint_interval = checkpoint_interval
        self.restore_model = model_checkpoint != ""
        self.model_checkpoint = model_checkpoint
        self.start_t = start_t

        img_h, img_w, img_c = self.env.observation_space.shape
        input_shape = (img_h, img_w, frame_history_len * img_c)

        self.num_actions = self.env.action_space.n

        # set up placeholders
        self.trace_length_ph = tf.placeholder(dtype=tf.int32)
        self.batch_size_ph = tf.placeholder(dtype=tf.int32, shape=[])
        #We take the output from the final convolutional layer and send it to a recurrent layer.
        #The input must be reshaped into [batch x trace x units] for rnn processing, 
        #and then returned to [batch x units] when sent through the upper levles.
        
        # placeholder for prioritized replay buffer weights
        self.ISWeights_ = tf.placeholder(tf.float32, [None,1], name='IS_weights')
        # placeholder for training phase
        self.train_phase = tf.placeholder(tf.bool, name="is_training")
        # placeholder for current observation (or state)
        self.obs_t_ph              = tf.placeholder(
            tf.float32, [None] + list(input_shape))
        # placeholder for current action
        self.act_t_ph              = tf.placeholder(tf.int32,   [None])
        # placeholder for the next action
        self.act_next_ph = tf.placeholder(tf.int32,   [None])
        # placeholder for current reward
        self.rew_t_ph              = tf.placeholder(tf.float32, [None])
        # placeholder for next observation (or state)
        self.obs_tp1_ph            = tf.placeholder(
            tf.float32, [None] + list(input_shape))
        # placeholder for end of episode mask
        # this value is 1 if the next state corresponds to the end of an episode,
        # in which case there is no Q-value at the next state; at the end of an
        # episode, only the current state reward contributes to the target, not the
        # next state Q-value (i.e. target is just rew_t_ph, not rew_t_ph + gamma * q_tp1)
        self.done_mask_ph          = tf.placeholder(tf.float32, [None])


        obs_t_float   = tf.cast(self.obs_t_ph,   tf.float32) 
        obs_tp1_float = tf.cast(self.obs_tp1_ph, tf.float32) 

        # Define the RNN cells. Only used if model uses rnn cells
        self.rnn_cell = tf.contrib.rnn.LSTMCell(name = "basic_lstm_cell", num_units = self.hsize, state_is_tuple=True)
        self.rnn_cell_t = tf.contrib.rnn.LSTMCell(name = "basic_lstm_cell", num_units = self.hsize, state_is_tuple=True)
        self.state_in = self.rnn_cell.zero_state(self.batch_size_ph, tf.float32) # Place holder
        self.state_in_t = self.rnn_cell_t.zero_state(self.batch_size_ph, tf.float32)

        # Training Q-func
        if self.rnn_model:
            self.q_t, self.rnn_state = q_func(obs_t_float, self.train_phase, self.batch_size_ph, self.trace_length_ph, self.state_in, self.rnn_cell, self.hsize,
                self.num_actions, self.dropout, self.batch_norm, scope = "q_func", reuse = tf.AUTO_REUSE)
        else:
            self.q_t = q_func(obs_t_float, self.train_phase, self.num_actions, self.dropout, self.batch_norm, scope = "q_func", reuse = tf.AUTO_REUSE)
        #q_t_next = q_func(obs_tp1_float, self.train_phase, self.num_actions, self.dropout, self.batch_norm, scope = "q_func", reuse = tf.AUTO_REUSE)

        # Target Q-func
        if self.rnn_model:
            q_target, self.rnn_state_target = q_func(obs_tp1_float, self.train_phase, self.batch_size_ph, self.trace_length_ph, self.state_in, self.rnn_cell_t, self.hsize,
                self.num_actions, self.dropout, self.batch_norm, scope = "target_q_func", reuse = tf.AUTO_REUSE)
        else:
            q_target = q_func(obs_tp1_float, self.train_phase, self.num_actions, self.dropout, self.batch_norm, scope = "target_q_func", reuse = False)

        # Best action operator
        self.best_action = tf.argmax(self.q_t, axis = 1)


        # Max predicted Q that is used in the Bellman backup operator
        if self.double_q:
          # If we're doing double q-learning we need to calculate the Bellman estimate in a different manner  
          max_q = tf.reduce_sum(tf.multiply(q_target, tf.one_hot(self.act_next_ph, self.num_actions)), axis = 1)
        else:
          max_q = tf.reduce_max(q_target, axis = 1)

        # Bellman backup operator
        y = self.rew_t_ph + gamma * tf.multiply((1.0 - self.done_mask_ph), max_q)

        # Predicted q value in the loss function
        q_t_pred = tf.reduce_sum(tf.multiply(self.q_t, tf.one_hot(self.act_t_ph, self.num_actions)), axis = 1)

        # Loss function. Different if using prioritized replay
        if not(self.prioritized_replay) and self.rnn_model:
            maskA = tf.zeros([self.batch_size_ph, self.trace_length_ph//2])
            maskB = tf.ones([self.batch_size_ph, self.trace_length_ph//2])
            mask = tf.concat([maskA, maskB],1)
            mask = tf.reshape(mask,[-1])
            self.total_error = tf.reduce_mean(mask * tf.squared_difference(q_t_pred, y))
        elif not(self.prioritized_replay):
            self.total_error = tf.losses.mean_squared_error(y, q_t_pred)
        else:
            self.absolute_error = tf.abs(y - q_t_pred)
            self.total_error = tf.reduce_mean(self.ISWeights_ * tf.squared_difference(q_t_pred, y))

        # Used to track the training progress of the Q-network
        self.q_t_best = tf.reduce_max(self.q_t, axis = 1)
        self.q_t_mean = tf.reduce_mean(self.q_t_best)

        # Collect update ops and variables
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope='q_func')
        target_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope='target_q_func')
        q_func_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='q_func')
        target_q_func_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='target_q_func')

        ######

        # construct optimization op (with gradient clipping)
        if not(runtime):
          self.learning_rate = tf.placeholder(tf.float32, (), name="learning_rate")
          optimizer = self.optimizer_spec.constructor(learning_rate=self.learning_rate, **self.optimizer_spec.kwargs)
          with tf.control_dependencies(update_ops): # Necessary when doing double-dqn
            self.train_fn = minimize_and_clip(optimizer, self.total_error,
                         var_list=q_func_vars, clip_val=grad_norm_clipping)

        # update_target_fn will be called periodically to copy Q network to target Q network
        update_target_fn = []
        for var, var_target in zip(sorted(q_func_vars,        key=lambda v: v.name),
                                   sorted(target_q_func_vars, key=lambda v: v.name)):
            update_target_fn.append(var_target.assign(var))
        self.update_target_fn = tf.group(*update_target_fn)

        # construct the replay buffer
        if prioritized_replay:
            self.replay_buffer = TreeBuffer(int(replay_buffer_size/2)) # We divide by half since the tree buffer uses twice more memory
        else:
            self.replay_buffer = ReplayBuffer(replay_buffer_size, frame_history_len)
            self.replay_buffer_idx = None

        # Create a model save file
        self.saver = tf.train.Saver()
        self.model_file = "models/model-" + session_name + '.cpkt'

        # Setup starting values for some parameters
        self.model_initialized = False
        self.num_param_updates = 0
        self.mean_episode_reward      = -float('nan')
        self.best_mean_episode_reward = -float('inf')
        self.best_mean_q_value = -float('inf')
        self.best_mean_sinr_improvement = -float('inf')
        self.mean_q_value = -float('nan')
        self.mean_sinr_improvement = -float('nan')
        self.last_obs = self.env.reset()
        self.q_estimate_obs_batch = []
        self.log_every_n_steps = 5000
        self.state = (np.zeros([1,self.hsize]),np.zeros([1,self.hsize])) # Used if the model has an rnn cell
        self.state_train = (np.zeros([batch_size,self.hsize]),np.zeros([batch_size,self.hsize]))

        self.start_time = time.time()
        self.t = 0



        # If runtime mode, initialize the variables
        if runtime or self.restore_model:
            self.restore(self.model_checkpoint)
            print("Model restored to time %d." % self.t)
            self.model_initialized = True
            self.t = self.start_t
            if not(self.runtime):
                self.create_logging_file()

    def stopping_criterion_met(self):
        return self.stopping_criterion is not None and self.stopping_criterion(self.env, self.t)

    def create_logging_file(self):

        self.logging_file = "logs/logging-" + self.session_name +'.log'
        f = open(self.logging_file, 'w')
        f.write('timestep, mean_reward, mean_sinr_improvement, mean_q_value, best_mean_reward, best_mean_q_value, best_mean_sinr_improvement, episodes, running_time, exploration\n')
        f.close()
        print('Created logging file:', self.logging_file)


    def restore(self, model_checkpoint):
        q_func_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='q_func')
        self.saver.restore(self.session, model_checkpoint)

    def get_q_t(self, obs):
        q_t = self.session.run(self.q_t, \
                 feed_dict={self.obs_t_ph: obs, self.trace_length_ph: 1, self.state_in:self.state, self.batch_size_ph:len(obs), self.train_phase: False})  
        return q_t  

    def runtime_step(self, action_randomness, uav_idx):
        obs = self.env.get_observation(uav_idx)
        if len(obs.shape) == 2:
            # We only have one channel, therfore we need to reshape the array
            obs = obs.reshape((obs.shape[0], obs.shape[1], 1))
        viable_actions = self.env.get_viable_actions(uav_idx)

        if self.rnn_model:
            q_t, action, state_temp = self.session.run([self.q_t, self.best_action, self.rnn_state], \
                     feed_dict={self.obs_t_ph: [obs], self.trace_length_ph: 1, self.state_in:self.state, self.batch_size_ph:1, self.train_phase: False})    
        else:
            q_t, action = self.session.run([self.q_t, self.best_action], feed_dict = {self.obs_t_ph: [obs], self.train_phase: False})   
        
        q_t = q_t.T
        q_t[viable_actions.T == 0] = -np.Inf            
        action = np.argmax(q_t)

        if random.random() < action_randomness * self.num_actions / (self.num_actions - 1):
            action = self.env.get_random_action(uav_idx)


        next_obs, reward, done, _ = self.env.step(action, uav_idx)

        if done:
            # The environment will get reset on the outside
            self.state = (np.zeros([1,self.hsize]),np.zeros([1,self.hsize]))
        else:
            #self.last_obs[uav_idx] = next_obs
            if self.rnn_model and self.model_initialized:
                self.state = state_temp

        return done 

    def reset_env(self):
        self.last_obs = self.env.reset()

    def step_env(self, runtime = False):
        idx = self.replay_buffer.store_frame(self.last_obs)
        
        if not self.model_initialized:
            action = self.env.get_random_action()

        else:
            obs = self.replay_buffer.encode_recent_observation()
            viable_actions = self.env.get_viable_actions()
            if self.rnn_model:
                q_t, state_temp = self.session.run([self.q_t, self.rnn_state], \
                         feed_dict={self.obs_t_ph: [obs], self.trace_length_ph: 1, self.state_in:self.state, self.batch_size_ph:1, self.train_phase: False})

                if random.random() < self.exploration.value(self.t) * self.num_actions / (self.num_actions - 1):
                    action = self.env.get_random_action()
                else:
                    q_t = q_t.T
                    q_t[viable_actions.T == 0] = -np.Inf            
                    action = np.argmax(q_t)
            else:
                if random.random() < self.exploration.value(self.t) * self.num_actions / (self.num_actions - 1):
                    action = self.env.get_random_action()
                else:
                    q_t = self.session.run(self.q_t, feed_dict = {self.obs_t_ph: [obs], self.train_phase: False})   
                    q_t = q_t.T
                    q_t[viable_actions.T == 0] = -np.Inf            
                    action = np.argmax(q_t)
                           

        next_obs, reward, done, _ = self.env.step(action)




        self.replay_buffer.store_effect(idx, action, reward, done)

        if done:
            self.last_obs = self.env.reset()
            self.state = (np.zeros([1,self.hsize]),np.zeros([1,self.hsize]))
        else:
            self.last_obs = next_obs
            if self.rnn_model and self.model_initialized:
                self.state = state_temp

    def update_model(self):
        # Perform experience replay and train the network.
        if ((self.t - self.start_t) > self.learning_starts and self.t % self.learning_freq == 0):

            if self.rnn_model:
                obs_batch, act_batch, rew_batch, next_obs_batch, done_batch = self.replay_buffer.sample_batches(self.batch_size, self.trace_size)
            else:
                obs_batch, act_batch, rew_batch, next_obs_batch, done_batch = self.replay_buffer.sample(self.batch_size)

            # Also collect collect a batch of states to estimate the q-value functions training progress
            if self.rnn_model and not(len(self.q_estimate_obs_batch)):
                self.q_estimate_obs_batch, _, _, _, _ = self.replay_buffer.sample_batches(self.batch_size, self.trace_size)
            elif not(len(self.q_estimate_obs_batch)):
                self.q_estimate_obs_batch, _, _, _, _ = self.replay_buffer.sample(5000)

            # Intialize the model
            if not self.model_initialized:
                #obs = self.q_estimate_obs_batch[4].astype(np.float)
                #print(obs.shape)
                #self.fig, (self.ax1, self.ax2) = plt.subplots(2, 1)
                #self.ax1.imshow(obs[:,:,0], cmap='plasma')
                #self.ax2.imshow(obs[:,:,1], cmap='plasma')
                #self.fig.show()
                #plt.pause(1000)
                self.create_logging_file()
                initialize_interdependent_variables(self.session, tf.global_variables(), {
                self.obs_t_ph: obs_batch,
                self.obs_tp1_ph: next_obs_batch,
                }) 
                self.model_initialized = True
                print("Model initialized at time %d." % self.t)

            best_next_actions = []
            if self.double_q:
                best_next_actions = self.session.run(self.best_action, \
                    feed_dict = {self.obs_t_ph: next_obs_batch, self.trace_length_ph: self.trace_size, self.state_in : self.state_train, self.batch_size_ph : self.batch_size, self.train_phase: False})

            self.session.run(self.train_fn, {
                self.train_phase: True,
                   self.obs_t_ph: obs_batch,
                    self.act_t_ph: act_batch,
                    self.rew_t_ph: rew_batch,
                    self.obs_tp1_ph: next_obs_batch,
                    self.done_mask_ph: done_batch,
                    self.batch_size_ph: self.batch_size,
                    self.trace_length_ph: self.trace_size,
                    self.state_in: self.state_train,
                    self.state_in_t: self.state_train,
                    self.act_next_ph: best_next_actions,
                    self.learning_rate: self.optimizer_spec.lr_schedule.value(self.t)
                })

            self.num_param_updates += 1

            if self.num_param_updates % self.target_update_freq == 0:
                print("Updating the target function for %dth time." % self.num_param_updates)
                self.session.run(self.update_target_fn)
                print("Target model updated at time %d." % self.t)

        self.t += 1

    def get_mean_q_value(self):
        # Estimate the mean q value
        if len(self.q_estimate_obs_batch):
            mean_q_value = self.session.run(self.q_t_mean, \
                feed_dict = {self.obs_t_ph: self.q_estimate_obs_batch, self.trace_length_ph: self.trace_size, self.state_in : self.state_train, self.batch_size_ph : self.batch_size, self.train_phase: False})
            self.best_mean_q_value = max(self.best_mean_q_value, mean_q_value)
        else:
            mean_q_value = self.mean_q_value
        return mean_q_value

    def log_progress(self):
        episode_rewards = self.env.get_episode_rewards()
        sinr_improvements = self.env.get_sinr_improvements()

        if len(episode_rewards) > 0:
            self.mean_episode_reward = np.mean(episode_rewards[-100:])
            self.mean_sinr_improvement = np.mean(sinr_improvements[-100:])


        if len(episode_rewards) > 100:
            if self.best_mean_sinr_improvement < self.mean_sinr_improvement:
                # Save model variables
                if self.model_initialized:
                    save_path = self.saver.save(self.session, self.model_file)
                    print("Model saved in path: %s" % save_path)
            self.best_mean_episode_reward = max(self.best_mean_episode_reward, self.mean_episode_reward)
            self.best_mean_sinr_improvement = max(self.best_mean_sinr_improvement, self.mean_sinr_improvement)

        if (self.t % self.log_every_n_steps == 0) and (self.t - self.start_t) > self.learning_starts:

            # Find the estimate of the Q-value
            self.mean_q_value = self.get_mean_q_value()

            print("Timestep %d" % (self.t,))
            print("mean reward (100 episodes) %f" % self.mean_episode_reward)
            print("mean sinr improvement (100 episodes)%f" % self.mean_sinr_improvement)
            print("best mean reward %f" % self.best_mean_episode_reward)
            print("best mean sinr improvement %f" % self.best_mean_sinr_improvement)
            print("mean q value %f" % self.mean_q_value)
            print("best mean q value %f" % self.best_mean_q_value)
            print("episodes %d" % len(episode_rewards))
            print("exploration %f" % self.exploration.value(self.t))
            print("learning_rate %f" % self.optimizer_spec.lr_schedule.value(self.t))
            if self.start_time is not None:
                running_time = (time.time() - self.start_time) / 60.
                print("running time %f" % (running_time))

            # Write data to a log file
            f = open(self.logging_file, 'a')
            f.write('%d, %f, %f, %f, %f, %f, %f, %d, %f, %f\n' % (self.t, self.mean_episode_reward, 
                                    self.mean_sinr_improvement, self.mean_q_value, self.best_mean_episode_reward, 
                                    self.best_mean_q_value, self.best_mean_sinr_improvement, len(episode_rewards), running_time, self.exploration.value(self.t)))
            f.close()

        if self.t % self.checkpoint_interval == 0 and self.model_initialized:
            # create a checkpoint for the algorithm
            print('Creating a checkpoint for the algorithm')
            #save_path = self.saver.save(self.session, self.model_file)
            #print("Model saved in path: %s" % save_path)

def learn(*args, **kwargs):
    alg = QLearner(*args, **kwargs)
    while not alg.stopping_criterion_met():
        alg.step_env()
        alg.update_model()
        alg.log_progress()


