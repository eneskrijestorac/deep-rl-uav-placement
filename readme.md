### The repository for the deep RL UAV placement project.

+ Files geometry.py, process_ray_trac_dat.py, environment.py generate the training environment
+ Files dqq.py, dqn_utils.py contain the deep RL algorithm definition. This code was developed by completing the homework assignments from the CS 294 course at Berkeley. The code was further customized after. 
+ train_dqn.py is used to train the algorithm. The deep neural network function approximators are defined here. 
+ test_dqn.py is used to test a trained model. The model must be specified within the file. 